package org.bj.academylmsapi.model.trainingManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.format.DateTimeFormatter;

// 훈련관리 - 훈련일지 등록 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegisterResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "강사 id")
    private Long teacherId;

    @Schema(description = "훈련날짜")
    private String dateTraining;

    @Schema(description = "재적(명)")
    private Short registrationNum;

    @Schema(description = "출석(명)")
    private Short attendanceNum;

    @Schema(description = "이론(시간)")
    private Short theory;

    @Schema(description = "실습(시간)")
    private Short practice;

    @Schema(description = "결석(사람)")
    private String absentWho;

    @Schema(description = "지각(사람)")
    private String lateWho;

    @Schema(description = "조퇴(사람)")
    private String earlyLeaveWho;

    @Schema(description = "담당강사")
    private String mainTeacher;

    @Schema(description = "훈련내용")
    private String trainingContent;

    @Schema(description = "기타사항")
    private String etc;

    @Schema(description = "등록일")
    private String dateRegister;

    private TrainingRegisterResponse(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.dateTraining = builder.dateTraining;
        this.registrationNum = builder.registrationNum;
        this.attendanceNum = builder.attendanceNum;
        this.theory = builder.theory;
        this.practice = builder.practice;
        this.absentWho = builder.absentWho;
        this.lateWho = builder.lateWho;
        this.earlyLeaveWho = builder.earlyLeaveWho;
        this.mainTeacher = builder.mainTeacher;
        this.trainingContent = builder.trainingContent;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegisterResponse> {
        private final Long id;
        private final Long teacherId;
        private final String dateTraining;
        private final Short registrationNum;
        private final Short attendanceNum;
        private final Short theory;
        private final Short practice;
        private final String absentWho;
        private final String lateWho;
        private final String earlyLeaveWho;
        private final String mainTeacher;
        private final String trainingContent;
        private final String etc;
        private final String dateRegister;

        public Builder(TrainingRegister trainingRegister) {
            this.id = trainingRegister.getId();
            this.teacherId = trainingRegister.getTeacher().getId();
            this.dateTraining = trainingRegister.getDateTraining().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.registrationNum = trainingRegister.getRegistrationNum();
            this.attendanceNum = trainingRegister.getAttendanceNum();
            this.theory = trainingRegister.getTheory();
            this.practice = trainingRegister.getPractice();
            this.absentWho = trainingRegister.getAbsentWho();
            this.lateWho = trainingRegister.getLateWho();
            this.earlyLeaveWho = trainingRegister.getEarlyLeaveWho();
            this.mainTeacher = trainingRegister.getTeacher().getTeacherName();
            this.trainingContent = trainingRegister.getTrainingContent();
            this.etc = trainingRegister.getEtc();
            this.dateRegister = trainingRegister.getDateRegister().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
        }

        @Override
        public TrainingRegisterResponse build() {
            return new TrainingRegisterResponse(this);
        }
    }
}
