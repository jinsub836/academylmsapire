package org.bj.academylmsapi.model.trainingManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegisterItem {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "강사 id")
    private Long teacherId;

    @Schema(description = "훈련날짜")
    private String dateTraining;

    @Schema(description = "재적(명)")
    private Short registrationNum;

    @Schema(description = "결석(명)")
    private Integer absentWho;

    @Schema(description = "지각(명)")
    private Integer lateWho;

    @Schema(description = "조퇴(명)")
    private Integer earlyLeaveWho;

    @Schema(description = "등록일")
    private String dateRegister;

    private TrainingRegisterItem(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.dateTraining = builder.dateTraining;
        this.registrationNum = builder.registrationNum;
        this.absentWho = builder.absentWho;
        this.lateWho = builder.lateWho;
        this.earlyLeaveWho = builder.earlyLeaveWho;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegisterItem> {
        private final Long id;
        private final Long teacherId;
        private final String dateTraining;
        private final Short registrationNum;
        private final Integer absentWho;
        private final Integer lateWho;
        private final Integer earlyLeaveWho;
        private final String dateRegister;

        public Builder(TrainingRegister trainingRegister) {
            this.id = trainingRegister.getId();
            this.teacherId = trainingRegister.getTeacher().getId();
            this.dateTraining = trainingRegister.getDateTraining().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.registrationNum = trainingRegister.getRegistrationNum();
            this.absentWho = trainingRegister.getAbsentWho().split("@").length;
            this.lateWho = trainingRegister.getLateWho().split("@").length;
            this.earlyLeaveWho = trainingRegister.getEarlyLeaveWho().split("@").length;
            this.dateRegister = trainingRegister.getDateRegister().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
        }


        @Override
        public TrainingRegisterItem build() {
            return new TrainingRegisterItem(this);
        }
    }
}
