package org.bj.academylmsapi.model.board.board;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.board.reply.ReplyResponse;

import java.time.LocalDateTime;

// 게시판 - 게시판 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "회원등급(id가 누군지 알기 위해 넣음)")
    private MemberType memberType;

    @Schema(description = "멤버 id(수강생 및 강사)")
    private Long memberId;

    @Schema(description = "게시글 제목")
    private String boardTitle;

    @Schema(description = "게시글 작성자")
    private String username;

    @Schema(description = "게시글 내용")
    private String boardContent;

    @Schema(description = "게시글 등록일")
    private LocalDateTime dateCreate;

    @Schema(description = "게시글 답변")
    private ReplyResponse comment;

    private BoardResponse(Builder builder) {
        this.id = builder.id;
        this.memberType = builder.memberType;
        this.memberId = builder.memberId;
        this.boardTitle = builder.boardTitle;
        this.username = builder.username;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
        this.comment = builder.comment;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final MemberType memberType;
        private final Long memberId;
        private final String boardTitle;
        private final String username;
        private final String boardContent;
        private final LocalDateTime dateCreate;
        private ReplyResponse comment;

        public Builder(Board board) {
            this.id = board.getId();
            this.memberType = board.getMemberType();
            this.memberId = board.getMemberId();
            this.boardTitle = board.getBoardTitle();
            this.username = board.getUsername();
            this.boardContent = board.getBoardContent();
            this.dateCreate = board.getDateCreate();
        }

        public Builder comment (Reply reply) {
            this.comment = reply == null ? null : new ReplyResponse.Builder(reply).build();
            return this;
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
