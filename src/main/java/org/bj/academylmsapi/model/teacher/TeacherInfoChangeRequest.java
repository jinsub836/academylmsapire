package org.bj.academylmsapi.model.teacher;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

// 강사 email U model

@Getter
@Setter
public class TeacherInfoChangeRequest {
    @Schema(description = "사진 주소")
    private String imgSrc;

    @NotNull
    @Schema(description = "이름")
    private String teacherName;

    @NotNull
    @Schema(description = "휴대폰 번호")
    private String phoneNumber;

    @NotNull
    @Schema(description = "주소")
    private String address;

    @NotNull
    @Schema(description = "이메일", maxLength = 50)
    @Length(max = 50)
    private String email;
}
