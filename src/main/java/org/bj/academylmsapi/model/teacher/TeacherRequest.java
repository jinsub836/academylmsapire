package org.bj.academylmsapi.model.teacher;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.Gender;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 C model

@Getter
@Setter
public class TeacherRequest {

    @NotNull
    @Schema(description = "이름", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String teacherName;

    @NotNull
    @Schema(description = "아이디", minLength = 5, maxLength = 20)
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Schema(description = "비밀번호", minLength = 8)
    @Length(min = 8)
    private String password;

    @NotNull
    @Schema(description = "비밀번호 확인", minLength = 8)
    @Length(min = 8)
    private String passwordRe;

    @NotNull
    @Schema(description = "성별")
    private Gender gender;

    @NotNull
    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @NotNull
    @Schema(description = "휴대폰번호", minLength = 13, maxLength = 13)
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address;

    @NotNull
    @Schema(description = "이메일", maxLength = 50)
    @Length(max = 50)
    private String email;
}
