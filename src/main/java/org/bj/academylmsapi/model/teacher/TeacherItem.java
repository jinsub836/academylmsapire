package org.bj.academylmsapi.model.teacher;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeacherItem {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "사진 주소")
    private String imgSrc;

    @Schema(description = "이름")
    private String teacherName;

    @Schema(description = "성별")
    private Gender gender;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "이메일")
    private String email;

    public TeacherItem(Builder builder) {
        this.id = builder.id;
        this.imgSrc = builder.imgSrc;
        this.teacherName = builder.teacherName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.email = builder.email;
    }

    public static class Builder implements CommonModelBuilder<TeacherItem> {
        private final Long id;
        private final String imgSrc;
        private final String teacherName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String email;

        public Builder(Teacher teacher) {
            this.id = teacher.getId();
            this.imgSrc = teacher.getImgSrc();
            this.teacherName = teacher.getTeacherName();
            this.gender = teacher.getGender();
            this.dateBirth = teacher.getDateBirth();
            this.email = teacher.getEmail();
        }

        @Override
        public TeacherItem build() {
            return new TeacherItem(this);
        }
    }
}
