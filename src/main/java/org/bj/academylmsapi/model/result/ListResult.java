package org.bj.academylmsapi.model.result;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

// List 반응 model

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    @Schema(description = "메세지")
    private String msg;

    @Schema(description = "코드")
    private Integer code;

    @Schema(description = "리스트")
    private List<T> list;

    @Schema(description = "총 데이터 개수")
    private Long totalCount; // 총 데이터개수

    @Schema(description = "총 페이지 수")
    private Integer totalPage; // 총 페이지수 -> 1 페이지

    @Schema(description = "현재 페이지 수")
    private Integer currentPage; // 현재 페이지번호 -1
}
