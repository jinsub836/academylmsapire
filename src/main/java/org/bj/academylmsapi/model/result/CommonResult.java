package org.bj.academylmsapi.model.result;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

// 그냥 반응 model

@Getter
@Setter
public class CommonResult {
    @Schema(description = "메세지")
    private String msg;
    @Schema(description = "코드")
    private Integer code;
    @Schema(description = "성공했니?")
    private Boolean isSuccess;

}
