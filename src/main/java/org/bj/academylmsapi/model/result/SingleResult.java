package org.bj.academylmsapi.model.result;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

// 단수 반응 model

@Getter
@Setter
public class SingleResult<T> extends CommonResult{
    @Schema(description = "메세지")
    private String msg;

    @Schema(description = "코드")
    private Integer code;

    @Schema(description = "데이터")
    private T data;
}
