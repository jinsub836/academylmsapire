package org.bj.academylmsapi.model.testManagement.subjectStatus;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import org.bj.academylmsapi.enums.TestType;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;


@Getter
@Setter
public class SubjectStatusRequest {
    @NotNull
    @Schema(description = "과목 id")
    private Long subjectId;

    @NotNull
    @Schema(description = "능력단위명", minLength = 2, maxLength = 20)
    private String abilityUnitName;

    @NotNull
    @Schema(description = "능력단위요소명", minLength = 2, maxLength = 20)
    private String abilityUnitFactorName;

    @NotNull
    @Schema(description = "NCS", minLength = 2, maxLength = 20)
    private String NCS;

    @NotNull
    @Schema(description = "평가명", minLength = 2, maxLength = 20)
    private String testName;

    @NotNull
    @Schema(description = "평가일")
    private LocalDate dateTest;

    @NotNull
    @Schema(description = "평가시간(분)")
    private Short testTime;

    @NotNull
    @Schema(description = "평가유형")
    private TestType testType;

    @NotNull
    @Schema(description = "수준(NCS 5단계)")
    private Short level;

    @NotNull
    @Schema(description = "총배점(100점)")
    private Short score;

    @NotNull
    @Schema(description = "문항수")
    private Short problemNum;
}
