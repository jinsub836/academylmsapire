package org.bj.academylmsapi.model.testManagement.subjectStatus;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.enums.TestType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED )
public class SubjectStatusResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "과목명")
    private String subjectName;

    @Schema(description = "능력단위명")
    private String abilityUnitName;

    @Schema(description = "능력단위요소명")
    private String abilityUnitFactorName;

    @Schema(description = "NCS")
    private String NCS;

    @Schema(description = "평가명")
    private String testName;

    @Schema(description = "평가일")
    private LocalDate dateTest;

    @Schema(description = "평가시간(분)")
    private Short testTime;

    @Schema(description = "평가유형")
    private TestType testType;

    @Schema(description = "출제여부")
    private String isSetTest; // 3항 사용을 위해 String type 으로 변환

    @Schema(description = "수준(NCS 5단계)")
    private Short level;

    @Schema(description = "총배점(100점)")
    private Short score;

    @Schema(description = "문항수")
    private Short problemNum;

    private SubjectStatusResponse (Builder builder){
        this.id = builder.id;
        this.subjectName= builder.subjectName;
        this.abilityUnitName=  builder.abilityUnitName;
        this.abilityUnitFactorName= builder.abilityUnitFactorName;
        this.NCS= builder.NCS;
        this.testName= builder.testName;
        this.dateTest= builder.dateTest;
        this.testTime= builder.testTime;
        this.testType= builder.testType;
        this.isSetTest= builder.isSetTest ;
        this.level= builder.level;
        this.score= builder.score;
        this.problemNum = builder.problemNum;
    }

    public static class Builder implements CommonModelBuilder<SubjectStatusResponse>{
        private final Long id;
        private final String subjectName;
        private final String abilityUnitName;
        private final String abilityUnitFactorName;
        private final String NCS;
        private final String testName;
        private final LocalDate dateTest;
        private final Short testTime;
        private final TestType testType;
        private final String isSetTest; // 3항 사용을 위해 String type 으로 변환
        private final Short level;
        private final Short score;
        private final Short problemNum;


        public Builder(SubjectStatus subjectStatus){
            this.id = subjectStatus.getId();
            this.subjectName= subjectStatus.getSubjectId().getSubjectName();
            this.abilityUnitName= subjectStatus.getAbilityUnitName();
            this.abilityUnitFactorName= subjectStatus.getAbilityUnitFactorName();
            this.NCS= subjectStatus.getNCS();
            this.testName= subjectStatus.getTestName();
            this.dateTest= subjectStatus.getDateTest();
            this.testTime= subjectStatus.getTestTime();
            this.testType= subjectStatus.getTestType();
            this.isSetTest = subjectStatus.getIsSetTest() ? "출제 완료" : "미출제";
            this.level= subjectStatus.getLevel();
            this.score= subjectStatus.getScore();
            this.problemNum = subjectStatus.getProblemNum();
        }
        @Override
        public SubjectStatusResponse build() {
            return new  SubjectStatusResponse(this);
        }
    }
}
