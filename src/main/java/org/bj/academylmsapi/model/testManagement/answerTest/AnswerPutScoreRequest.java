package org.bj.academylmsapi.model.testManagement.answerTest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerPutScoreRequest {
    @Schema(description = "교수자총평")
    private String teacherComment;

    @Schema(description = "평가점수")
    private Short studentScore;

    @Schema(description = "성취수준")
    private Short achieveLevel;
}
