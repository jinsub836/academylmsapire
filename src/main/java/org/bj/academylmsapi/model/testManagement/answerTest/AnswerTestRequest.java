package org.bj.academylmsapi.model.testManagement.answerTest;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TestStatus;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class AnswerTestRequest {
    @NotNull
    @Schema(description = "시험출제 id")
    private Long makeTest;

    @NotNull
    @Schema(description = "답변")
    private String testAnswer;

    @Schema(description = "답변 파일")
    private String addFile;
}
