package org.bj.academylmsapi.model.testManagement.makeTest;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MakeTestResponse {
    @NotNull
    @Schema(description = "id")
    private Long id;

    @NotNull
    @Schema(description = "과목현황 id")
    private Long subjectStatusId;

    @Schema(description = "시험문제")
    private  String[] testProblem;

    @Schema(description = "객관식")
    private List<String[]> testSelect;

    @NotNull
    @Schema(description = "등록일")
    private LocalDateTime dateRegister;

    private MakeTestResponse(Builder builder){
        this.id = builder.id;
        this.subjectStatusId = builder.subjectStatusId;
        this.testProblem = builder.testProblem;
        this.testSelect = builder.testSelect;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<MakeTestResponse>{
        private final Long id;
        private final Long subjectStatusId;
        private final String[] testProblem;
        private final List<String[]> testSelect;
        private final LocalDateTime dateRegister;

        public Builder(MakeTest makeTest){
            this.id = makeTest.getId();
            this.subjectStatusId = makeTest.getSubjectStatus().getId();
            this.testProblem = makeTest.getTestProblem().split("@");
            String[] strings =  makeTest.getTestSelect().split("@");
            List<String[]> strings2 = new LinkedList<>();
            for (int i = 0; i < strings.length; i++) strings2.add( strings[i].split("#"));
            this.testSelect = strings2;
            this.dateRegister = makeTest.getDateRegister();
        }

        @Override
        public MakeTestResponse build() {
            return new  MakeTestResponse(this);
        }
    }
}
