package org.bj.academylmsapi.model.testManagement.answerTest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.enums.TestStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AnswerTestItem {
    @Schema(description = "시험채점 id")
    private Long id;

    @Schema(description = "시험출제 id")
    private String makeTest;

    @Schema(description = "수강생명")
    private String studentName;

    @Schema(description = "응시현황")
    private String testStatus;

    private AnswerTestItem(Builder builder) {
        this.id = builder.id;
        this.makeTest = builder.makeTest;
        this.studentName = builder.studentName;
        this.testStatus = builder.testStatus;
    }

    public static class Builder implements CommonModelBuilder<AnswerTestItem> {
        private final Long id;
        private final String makeTest;
        private final String studentName;
        private final String testStatus;

        public Builder(AnswerTest answerTest) {
            this.id = answerTest.getId();
            this.makeTest = answerTest.getMakeTest().getSubjectStatus().getTestName();
            this.studentName = answerTest.getStudent().getStudentName();
            this.testStatus = answerTest.getTestStatus().getTestStatus();
        }

        @Override
        public AnswerTestItem build() {
            return new AnswerTestItem(this);
        }
    }
}
