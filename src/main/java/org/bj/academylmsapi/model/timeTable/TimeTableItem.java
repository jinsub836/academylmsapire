package org.bj.academylmsapi.model.timeTable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.timeTable.TimeTable;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor
public class TimeTableItem {
    @Schema(description = "시간표 id")
    private Long id;

    @Schema(description = "과정명")
    private String subjectName;

    @Schema(description = "훈련일자")
    private String dateTraining;

    @Schema(description = "훈련시작시간")
    private String startTimeTrain;

    @Schema(description = "훈련종료시간")
    private String endTime;

    @Schema(description = "방학/원격여부")
    private String isRemote;

    @Schema(description = "시작시간")
    private String startTime;

    @Schema(description = "시간구분")
    private String timeSorted;

    @Schema(description = "훈련강사코드")
    private String teacherCode;

    @Schema(description = "교육장소(강의실)코드")
    private String placeCode;

    @Schema(description = "교과목(및 능력단위) 코드")
    private String subjectCode;

    private TimeTableItem(Builder builder){
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.dateTraining = builder.dateTraining;
        this.startTimeTrain = builder.startTimeTrain;
        this.endTime = builder.endTime;
        this.isRemote = builder.isRemote;
        this.startTime = builder.startTime;
        this.timeSorted = builder.timeSorted;
        this.teacherCode = builder.teacherCode;
        this.placeCode = builder.placeCode;
        this.subjectCode = builder.subjectCode;

    }

    public static class Builder implements CommonModelBuilder<TimeTableItem>{

        private final Long id;
        private final String subjectName;
        private final String dateTraining;
        private final String startTimeTrain;
        private final String endTime;
        private final String isRemote;
        private final String startTime;
        private final String timeSorted;
        private final String teacherCode;
        private final String placeCode;
        private final String subjectCode;

        public Builder(TimeTable timeTable){
            this.id = timeTable.getId();
            this.subjectName = timeTable.getSubject().getSubjectName();
            this.dateTraining = timeTable.getDateTraining();
            this.startTimeTrain = timeTable.getStartTimeTrain();
            this.endTime = timeTable.getEndTime();
            this.isRemote = timeTable.getIsRemote();
            this.startTime = timeTable.getStartTime();
            this.timeSorted = timeTable.getTimeSorted();
            this.teacherCode = timeTable.getTeacherCode();
            this.placeCode = timeTable.getPlaceCode();
            this.subjectCode = timeTable.getSubjectCode();
        }

        @Override
        public TimeTableItem build() {
            return new TimeTableItem(this);
        }
    }
}
