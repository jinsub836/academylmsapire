package org.bj.academylmsapi.model.studentManagement.workManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.WorkManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 취업관리사항 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkManagementResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "업체명")
    private String companyName;

    @Schema(description = "사업자등록번호")
    private String businessRegisterNumber;

    @Schema(description = "취업확인서")
    private String employmentConfirmationForm;

    @Schema(description = "회사주소")
    private String companyAddress;

    @Schema(description = "회사연락처")
    private String companyCallNumber;

    @Schema(description = "근무직종")
    private String workField;

    @Schema(description = "고용보험유무")
    private String employmentInsuranceStatus;

    @Schema(description = "취업일")
    private LocalDate dateEmployment;

    private WorkManagementResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.companyName = builder.companyName;
        this.businessRegisterNumber = builder.businessRegisterNumber;
        this.employmentConfirmationForm = builder.employmentConfirmationForm;
        this.companyAddress = builder.companyAddress;
        this.companyCallNumber = builder.companyCallNumber;
        this.workField = builder.workField;
        this.employmentInsuranceStatus = builder.employmentInsuranceStatus;
        this.dateEmployment = builder.dateEmployment;
    }

    public static class Builder implements CommonModelBuilder<WorkManagementResponse> {
        private final Long id;
        private final Long studentId;
        private final String companyName;
        private final String businessRegisterNumber;
        private final String employmentConfirmationForm;
        private final String companyAddress;
        private final String companyCallNumber;
        private final String workField;
        private final String employmentInsuranceStatus;
        private final LocalDate dateEmployment;

        public Builder(WorkManagement workManagement) {
            this.id = workManagement.getId();
            this.studentId = workManagement.getStudent().getId();
            this.companyName = workManagement.getCompanyName();
            this.businessRegisterNumber = workManagement.getBusinessRegisterNumber();
            this.employmentConfirmationForm = workManagement.getEmploymentConfirmationForm();
            this.companyAddress = workManagement.getCompanyAddress();
            this.companyCallNumber = workManagement.getCompanyCallNumber();
            this.workField = workManagement.getWorkField();
            this.employmentInsuranceStatus = workManagement.getEmploymentInsuranceStatus();
            this.dateEmployment = workManagement.getDateEmployment();
        }
        @Override
        public WorkManagementResponse build() {
            return new WorkManagementResponse(this);
        }
    }
}
