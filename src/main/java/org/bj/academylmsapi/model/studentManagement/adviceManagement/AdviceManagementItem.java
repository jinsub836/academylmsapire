package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;


// 학생관리 - 취업관리 - 상담관리 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagementItem {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "수강생명")
    private String studentName;

    @Schema(description = "상담제목")
    private String adviceTitle;

    @Schema(description = "상담날짜")
    private LocalDate dateAdvice;

    private AdviceManagementItem(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.studentName = builder.studentName;
        this.adviceTitle = builder.adviceTitle;
        this.dateAdvice = builder.dateAdvice;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagementItem> {
        private final Long id;
        private final Long studentId;
        private final String studentName;
        private final String adviceTitle;
        private final LocalDate dateAdvice;

        public Builder(AdviceManagement adviceManagement) {
            this.id = adviceManagement.getId();
            this.studentId = adviceManagement.getStudent().getId();
            this.studentName = adviceManagement.getStudent().getStudentName();
            this.adviceTitle = adviceManagement.getAdviceTitle();
            this.dateAdvice = adviceManagement.getDateAdvice();
        }

        @Override
        public AdviceManagementItem build() {
            return new AdviceManagementItem(this);
        }
    }
}
