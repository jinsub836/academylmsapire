package org.bj.academylmsapi.model.studentManagement.career;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 경력사항 C model

@Getter
@Setter
public class CareerRequest {
    @NotNull
    @Schema(description = "수강생 id", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private Long studentId;

    @Schema(description = "근무처", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String office;

    @Schema(description = "근무기간", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String dateWork;

    @Schema(description = "직위", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String position;

    @Schema(description = "담당업무", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String whatTask;

    @Schema(description = "급여수준", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String payLevel;

    @Schema(description = "보유자격증", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String myLicense;

    @Schema(description = "고용지원금 대상자 여부", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String employSupport;

    @Schema(description = "소속기간명", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String belongOrganization;

    @Schema(description = "워크넷 취업활동 여부", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String workNetJobSearchStatus;

    @Schema(description = "보유기술", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String haveSkill;

    @Schema(description = "기타특이사항", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String etc;
}
