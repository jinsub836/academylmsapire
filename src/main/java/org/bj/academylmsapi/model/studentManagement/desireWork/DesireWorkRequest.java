package org.bj.academylmsapi.model.studentManagement.desireWork;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.hibernate.validator.constraints.Length;

// 학생관리 - 취업관리 - 희망근무조건 C model

@Getter
@Setter
public class DesireWorkRequest {
    @NotNull
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "희망지역", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String hopeArea;

    @Schema(description = "취업희망분야", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String hopeWorkField;

    @Schema(description = "희망급여수준", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String hopePayLevel;

    @Schema(description = "가능고용형태", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String possibleEmployForm;

    @Schema(description = "가능근무형태", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String possibleWorkForm;

    @Schema(description = "가능근무시간", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String possibleWorkTime;

    @Schema(description = "수강생 요청사항", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String studentRequest;
}
