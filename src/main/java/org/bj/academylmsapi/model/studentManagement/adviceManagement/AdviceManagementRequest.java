package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 C model

@Getter
@Setter
public class AdviceManagementRequest {
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "상담제목", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String adviceTitle;

    @Schema(description = "상담날짜")
    private LocalDate dateAdvice;

    @Schema(description = "상담내용")
    private String adviceContent;
}
