package org.bj.academylmsapi.model.subject;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TrainingType;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 과정 C model

@Getter
@Setter
public class SubjectRequest {

    @NotNull
    @Schema(description = "강사 id")
    private Long teacherId;

    @NotNull
    @Schema(description = "과정명", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String subjectName;

    @NotNull
    @Schema(description = "교육 시작일")
    private LocalDate dateStart;

    @NotNull
    @Schema(description = "교육 종료일")
    private LocalDate dateEnd;

    @NotNull
    @Schema(description = "훈련유형")
    private TrainingType trainingType;

    @NotNull
    @Schema(description = "회차")
    private Integer classTurn;

    @NotNull
    @Schema(description = "재적인원")
    private Integer registerHuman;

    @NotNull
    @Schema(description = "총 인원수")
    private Integer totalHuman;

    @NotNull
    @Schema(description = "ncs 수준")
    private Integer ncsLevel;
}
