package org.bj.academylmsapi.model.subject;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.enums.TrainingType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

// 과정 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SubjectItem {
    @Schema(description = "과정 id")
    private Long id;

    @Schema(description = "강사 id")
    private Long teacherId;

    @Schema(description = "과정명")
    private String subjectName;

    @Schema(description = "교육 시작일")
    private LocalDate dateStart;

    @Schema(description = "교육 종료일")
    private LocalDate dateEnd;

    @Schema(description = "교육기간")
    private Long periodTime;

    @Schema(description = "훈련유형")
    private TrainingType trainingType;

    @Schema(description = "회차")
    private Integer classTurn;

    @Schema(description = "재적인원")
    private Integer registerHuman;

    @Schema(description = "총 인원수")
    private Integer totalHuman;

    @Schema(description = "담당강사")
    private String mainTeacher;

    @Schema(description = "ncs 수준")
    private Integer ncsLevel;

    private SubjectItem(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.subjectName = builder.subjectName;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.periodTime = builder.periodTime;
        this.trainingType = builder.trainingType;
        this.classTurn = builder.classTurn;
        this.registerHuman = builder.registerHuman;
        this.totalHuman = builder.totalHuman;
        this.mainTeacher = builder.mainTeacher;
        this.ncsLevel = builder.ncsLevel;
    }

    public static class Builder implements CommonModelBuilder<SubjectItem> {
        private final Long id;
        private final Long teacherId;
        private final String subjectName;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final Long periodTime;
        private final TrainingType trainingType;
        private final Integer classTurn;
        private final Integer registerHuman;
        private final Integer totalHuman;
        private final String mainTeacher;
        private final Integer ncsLevel;

        public Builder(Subject subject) {
            this.id = subject.getId();
            this.teacherId = subject.getTeacher().getId();
            this.subjectName = subject.getSubjectName();
            this.dateStart = subject.getDateStart();
            this.dateEnd = subject.getDateEnd();
            this.periodTime = ChronoUnit.DAYS.between(subject.getDateStart(), subject.getDateEnd());
            this.trainingType = subject.getTrainingType();
            this.classTurn = subject.getClassTurn();
            this.registerHuman = subject.getRegisterHuman();
            this.totalHuman = subject.getTotalHuman();
            this.mainTeacher = subject.getTeacher().getTeacherName();
            this.ncsLevel = subject.getNcsLevel();
        }

        @Override
        public SubjectItem build() {
            return new SubjectItem(this);
        }
    }
}

