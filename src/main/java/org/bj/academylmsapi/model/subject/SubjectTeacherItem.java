package org.bj.academylmsapi.model.subject;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.enums.TrainingType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

// 과정 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SubjectTeacherItem {
    @Schema(description = "과정 id")
    private Long id;


    @Schema(description = "과정명")
    private String subjectName;

    @Schema(description = "교육 시작일")
    private LocalDate dateStart;

    @Schema(description = "교육 종료일")
    private LocalDate dateEnd;

    @Schema(description = "교육기간")
    private Long periodTime;

    @Schema(description = "회차")
    private Integer classTurn;

    @Schema(description = "재적인원")
    private Integer registerHuman;


    private SubjectTeacherItem(Builder builder) {
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.periodTime = builder.periodTime;
        this.classTurn = builder.classTurn;
        this.registerHuman = builder.registerHuman;
    }

    public static class Builder implements CommonModelBuilder<SubjectTeacherItem> {
        private final Long id;
        private final String subjectName;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final Long periodTime;
        private final Integer classTurn;
        private final Integer registerHuman;
        public Builder(Subject subject) {
            this.id = subject.getId();
            this.subjectName = subject.getSubjectName();
            this.dateStart = subject.getDateStart();
            this.dateEnd = subject.getDateEnd();
            this.periodTime = ChronoUnit.DAYS.between(subject.getDateStart(), subject.getDateEnd());
            this.classTurn = subject.getClassTurn();
            this.registerHuman = subject.getRegisterHuman();
        }

        @Override
        public SubjectTeacherItem build() {
            return new SubjectTeacherItem(this);
        }
    }
}

