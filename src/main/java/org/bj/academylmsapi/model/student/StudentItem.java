package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import java.time.LocalDate;

// 수강생 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentItem {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "과정 id")
    private Long subjectId;

    @Schema(description = "사진 주소")
    private String imgSrc;

    @Schema(description = "이름")
    private String studentName;

    @Schema(description = "성별")
    private Gender gender;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "본인 휴대폰번호")
    private String phoneNumber;

    @Schema(description = "수강상태")
    private StudentStatus studentStatus;

    @Schema(description = "상태 변경일")
    private LocalDate dateStatusChange;

    @Schema(description = "상태 변경 사유")
    private String statusWhy;

    private StudentItem(Builder builder) {
        this.id = builder.id;
        this.subjectId = builder.subjectId;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhy = builder.statusWhy;
    }

    public static class Builder implements CommonModelBuilder<StudentItem> { // 이거만 쓰고 alt + enter 하면 @over부터 나옴
        private final Long id;
        private final Long subjectId;
        private final String imgSrc;
        private final String studentName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhy;

        public Builder(Student student) {
            this.id = student.getId();
            this.subjectId = student.getSubject().getId();
            this.imgSrc = student.getImgSrc();
            this.studentName = student.getStudentName();
            this.gender = student.getGender();
            this.dateBirth = student.getDateBirth();
            this.phoneNumber = student.getPhoneNumber();
            this.studentStatus = student.getStudentStatus();
            this.dateStatusChange = student.getDateStatusChange();
            this.statusWhy = student.getStatusWhy();
        }

        @Override
        public StudentItem build() {
            return new StudentItem(this);
        }
    }
}
