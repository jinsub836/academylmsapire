package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 email U model

@Getter
@Setter
public class StudentInfoChangeRequest {
    @NotNull
    @Schema(description = "학생이름" , minLength = 2 , maxLength = 20)
    @Length(min = 2, max = 20)
    private String studentName;

    @NotNull
    @Schema(description = "본인 휴대폰번호", minLength = 13, maxLength = 14)
    @Length(max = 14)
    private String phoneNumber;

    @NotNull
    @Schema(description = "생년월일")
    private String dateBirth;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address1;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address2;

    @NotNull
    @Schema(description = "이메일" , maxLength = 50)
    @Length(max = 50)
    private String email;
}
