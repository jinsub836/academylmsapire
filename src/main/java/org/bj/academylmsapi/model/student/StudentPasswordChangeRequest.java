package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

// 수강생 비밀번호 변경 Request

@Getter
@Setter
public class StudentPasswordChangeRequest {
    @NotNull
    @Schema(description = "수강생 비밀번호" , minLength = 8)
    @Length(min = 8)
    private String password;
}
