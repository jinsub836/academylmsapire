package org.bj.academylmsapi.model.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 관리자 단수 R model

@Getter
@Setter
public class AdminResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "관리자이름")
    private String adminName;

    @Schema(description = "성별", enumAsRef = true)
    private Gender gender;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "휴대폰 번호")
    private String phoneNumber;

    private AdminResponse(Builder builder) {
        this.id = builder.id;
        this.adminName = builder.adminName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<AdminResponse> {
        private final Long id;
        private final String adminName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;

        public Builder(Admin admin) {
            this.id = admin.getId();
            this.adminName = admin.getAdminName();
            this.gender = admin.getGender();
            this.dateBirth = admin.getDateBirth();
            this.phoneNumber = admin.getPhoneNumber();
        }

        @Override
        public AdminResponse build() {
            return new AdminResponse(this);
        }
    }
}
