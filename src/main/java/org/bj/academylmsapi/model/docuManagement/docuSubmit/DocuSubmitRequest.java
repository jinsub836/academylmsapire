package org.bj.academylmsapi.model.docuManagement.docuSubmit;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

// 서류관리 - 서류제출 C model

@Getter
@Setter
public class DocuSubmitRequest {
    @NotNull
    @Schema(description = "서류등록 id")
    private Long signRegisterId;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "서명파일")
    private String signFile;

    @NotNull
    @Schema(description = "첨부파일")
    private String addFile;

    @NotNull
    @Schema(description = "서명완료(여부)")
    private Boolean isSignClear;
}
