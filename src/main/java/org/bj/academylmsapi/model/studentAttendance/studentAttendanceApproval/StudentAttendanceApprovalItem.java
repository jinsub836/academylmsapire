package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

// 수강생 출결 승인 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalItem {
    @Schema(description = "수강생 출결 승인 id")
    private Long id;

    @Schema(description = "수강반 이름")
    private String subjectName;

    @Schema(description = "수강생 이름")
    private String studentName;

    @Schema(description = "출결승인 유형")
    private String attendanceType;

    @Schema(description = "승인상태")
    private String approvalState;

    @Schema(description = "등록일")
    private LocalDate dateRegister;

    private StudentAttendanceApprovalItem(Builder builder) {
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.studentName = builder.studentName;
        this.attendanceType = builder.attendanceType;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalItem> {
        private final Long id;
        private final String subjectName;
        private final String studentName;
        private final String attendanceType;
        private final String approvalState;
        private final LocalDate dateRegister;

        public Builder(StudentAttendanceApproval studentAttendanceApproval) {
            this.id = studentAttendanceApproval.getId();
            this.subjectName = studentAttendanceApproval.getStudent().getSubject().getSubjectName();
            this.studentName = studentAttendanceApproval.getStudent().getStudentName();
            this.attendanceType = studentAttendanceApproval.getAttendanceType().getAttendanceTypeName();
            this.approvalState = studentAttendanceApproval.getApprovalState().getApprovalStateName();
            this.dateRegister = studentAttendanceApproval.getDateRegister();
        }

        @Override
        public StudentAttendanceApprovalItem build() {
            return new StudentAttendanceApprovalItem(this);
        }
    }
}
