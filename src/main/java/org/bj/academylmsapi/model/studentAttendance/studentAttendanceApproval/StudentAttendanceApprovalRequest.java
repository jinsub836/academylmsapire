package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

// 수강생 출결 승인 C model

@Getter
@Setter
public class StudentAttendanceApprovalRequest {
    @Schema(description = "시작일")
    private String dateStart;

    @Schema(description = "종료일")
    private String dateEnd;

    @Schema(description = "사유")
    private String reason;

    @Schema(description = "출결승인 유형")
    private String attendanceType;
}
