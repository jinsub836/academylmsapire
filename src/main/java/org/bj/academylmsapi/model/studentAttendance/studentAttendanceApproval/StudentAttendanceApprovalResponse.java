package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 출결 승인 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalResponse {
    @Schema(description = "수강생 출결 승인 id")
    private Long id;

    @Schema(description = "수강반 이름")
    private String subjectName;

    @Schema(description = "수강생 이름")
    private String studentName;

    @Schema(description = "수강생 username")
    private String studentId;

    @Schema(description = "출결승인 유형")
    private String attendanceType;

    @Schema(description = "사유", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String reason;

    @Schema(description = "시작일")
    private LocalDate dateStart;

    @Schema(description = "종료일")
    private LocalDate dateEnd;

    @Schema(description = "첨부 파일")
    private String addFile;

    @Schema(description = "승인상태")
    private String approvalState;

    @Schema(description = "등록일")
    private LocalDate dateRegister;

    private StudentAttendanceApprovalResponse(Builder builder) {
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.studentName = builder.studentName;
        this.studentId = builder.studentId;
        this.attendanceType = builder.attendanceType;
        this.reason = builder.reason;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.addFile = builder.addFile;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalResponse> {
        private final Long id;
        private final String subjectName;
        private final String studentName;
        private final String studentId;
        private final String attendanceType;
        private final String reason;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final String addFile;
        private final String approvalState;
        private final LocalDate dateRegister;

        public Builder(StudentAttendanceApproval studentAttendanceApproval) {
            this.id = studentAttendanceApproval.getId();
            this.subjectName = studentAttendanceApproval.getStudent().getSubject().getSubjectName();
            this.studentName = studentAttendanceApproval.getStudent().getStudentName();
            this.studentId = studentAttendanceApproval.getStudent().getUsername();
            this.attendanceType = studentAttendanceApproval.getAttendanceType().getAttendanceTypeName();
            this.reason = studentAttendanceApproval.getReason();
            this.dateStart = studentAttendanceApproval.getDateStart();
            this.dateEnd = studentAttendanceApproval.getDateEnd();
            this.addFile = studentAttendanceApproval.getAddFile();
            this.approvalState = studentAttendanceApproval.getApprovalState().getApprovalStateName();
            this.dateRegister = studentAttendanceApproval.getDateRegister();
        }

        @Override
        public StudentAttendanceApprovalResponse build() {
            return new StudentAttendanceApprovalResponse(this);
        }
    }
}
