package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.ApprovalState;

// 수강생 출결 승인 승인상태 U model

@Getter
@Setter
public class StudentAttendanceApprovalStateFixRequest {
    @Schema(description = "승인상태")
    private ApprovalState approvalState;
}
