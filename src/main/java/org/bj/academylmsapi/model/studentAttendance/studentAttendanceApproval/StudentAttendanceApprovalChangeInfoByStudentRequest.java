package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.AttendanceType;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class StudentAttendanceApprovalChangeInfoByStudentRequest {

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "시작일")
    private LocalDate dateStart;

    @Schema(description = "종료일")
    private LocalDate dateEnd;

    @Schema(description = "사유", maxLength = 30)
    @Length(min = 2, max = 30)
    private String reason;

    @Schema(description = "출결승인 유형")
    private AttendanceType attendanceType;
}
