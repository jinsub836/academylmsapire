package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalMonthly {
    @Schema(description = "승인 요청서 아이디")
    private Long id;

    @Schema(description = "기간")
    private String dateStartPeriod;

    @Schema(description = "승인상태")
    private String approvalState;

    @Schema(description = "출결승인 유형")
    private String attendanceType;

    @Schema(description = "등록일")
    private LocalDate dateRegister;

    private StudentAttendanceApprovalMonthly(Builder builder){
        this.id = builder.id;
        this.dateStartPeriod = builder.dateStartPeriod;
        this.approvalState = builder.approvalState;
        this.attendanceType = builder.attendanceType;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalMonthly>{
        private final Long id;
        private final String dateStartPeriod;
        private final String approvalState;
        private final String attendanceType;
        private final LocalDate dateRegister;

        public Builder(StudentAttendanceApproval studentAttendanceApproval){
            this.id = studentAttendanceApproval.getId();
            this.dateStartPeriod = studentAttendanceApproval.getDateStart()+"~"+studentAttendanceApproval.getDateEnd();
            this.approvalState = studentAttendanceApproval.getApprovalState().getApprovalStateName();
            this.attendanceType = studentAttendanceApproval.getAttendanceType().getAttendanceTypeName();
            this.dateRegister = studentAttendanceApproval.getDateRegister();
        }

        @Override
        public StudentAttendanceApprovalMonthly build() {
            return new StudentAttendanceApprovalMonthly(this);
        }
    }
}
