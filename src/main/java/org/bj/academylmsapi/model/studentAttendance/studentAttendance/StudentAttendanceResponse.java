package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.temporal.ChronoUnit;

// 수강생 출결 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceResponse {
    @Schema(description = "총 출석일(과목 출석일)")
    private Integer studentAttendance;

    @Schema(description = "나의 출석일(수강생 개개인 기준)")
    private Integer myStudentAttendance;

    @Schema(description = "조퇴")
    private Integer studentOut;

    @Schema(description = "지각")
    private Integer studentLate;

    @Schema(description = "무단결석")
    private Integer studentAbsence;

    @Schema(description = "병가")
    private Integer studentSick;

    @Schema(description = "출석률")
    private Long attendanceRate;

    @Schema(description = "훈련과정명")
    private String subjectName;

    @Schema(description = "기관명")
    private String academeName;

    @Schema(description = "훈련기간(시작일~종강일)")
    private String datePeriod;

    @Schema(description = "훈련진행률")
    private String progressRate;

    private StudentAttendanceResponse(Builder builder) {
        this.studentAttendance = builder.studentAttendance;
        this.myStudentAttendance = builder.myStudentAttendance;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsence = builder.studentAbsence;
        this.studentSick = builder.studentSick;
        this.attendanceRate = builder.attendanceRate;
        this.subjectName = builder.subjectName;
        this.academeName = builder.academeName;
        this.datePeriod = builder.datePeriod;
        this.progressRate = builder.progressRate;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceResponse> {
        private final Integer studentAttendance;
        private final Integer myStudentAttendance;
        private final Integer studentOut;
        private final Integer studentLate;
        private final Integer studentAbsence;
        private final Integer studentSick;
        private final Long attendanceRate;
        private final String subjectName;
        private final String academeName;
        private final String datePeriod;
        private final String progressRate;

        public Builder(StudentAttendance studentAttendance) {
            int studentReal =  studentAttendance.getStudentAttendance() - studentAttendance.getStudentAbsence()
                    -studentAttendance.getStudentLate()/3 -studentAttendance.getStudentOut()/3;
            double studentAttendRate = ((double)studentReal / (studentAttendance.getStudentAttendance()))*100;

            this.subjectName = studentAttendance.getStudent().getSubject().getSubjectName();
            this.studentAttendance = studentAttendance.getStudentAttendance();
            this.myStudentAttendance = studentReal;
            this.studentOut = studentAttendance.getStudentOut();
            this.studentLate = studentAttendance.getStudentLate();
            this.studentAbsence = studentAttendance.getStudentAbsence();
            this.studentSick = studentAttendance.getStudentSick();
            this.attendanceRate = Math.round(studentAttendRate);
            this.academeName = "라인아카데미";
            this.datePeriod = studentAttendance.getStudent().getSubject().getDateStart() + "~"
                    +studentAttendance.getStudent().getSubject().getDateEnd();
            this.progressRate = studentAttendance.getStudentAttendance() +"/"
                    + ChronoUnit.DAYS.between(studentAttendance.getStudent().getSubject().getDateStart(), studentAttendance.getStudent().getSubject().getDateEnd());
        }

        @Override
        public StudentAttendanceResponse build() {
            return new StudentAttendanceResponse(this);
        }
    }
}
