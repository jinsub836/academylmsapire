package org.bj.academylmsapi.model.login;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @Schema(description = "토큰")
    private String token;

    @Schema(description = "이름")
    private String name;

    private LoginResponse(Builder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public Builder(String token, String name) {
            this.token = token;
            this.name = name;
        }
        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}