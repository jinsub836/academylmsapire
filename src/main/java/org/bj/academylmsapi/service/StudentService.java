package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.student.*;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

// 수강생 service

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final StudentAttendanceService studentAttendanceService;
    private final SubjectRepository subjectRepository;
    private final ProfileService profileService;
    private final PasswordEncoder passwordEncoder;
    private final GCSService gcsService;

    // id 찾아오기
    public Student getData(long id) {
        return studentRepository.findById(id).orElseThrow();
    }

    // 수강생 C
    public void setStudent(StudentRequest request) throws IOException {
        Subject subjectData = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberException(); // 아이디 형식 검사
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberException(); // 비밀번호 재검
        if (!usernameDupCheck(request.getUsername())) throw new CMemberException(); // 아이디 중복 체크

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Student student = new Student.Builder(request, subjectData).build();
        studentRepository.save(student);

        studentAttendanceService.SetStudentAttendance(student.getId());
    }

/*    // 수강생 C 이미지 있음
    public void setStudent(StudentRequest request , MultipartFile multipartFile) throws IOException {
        Subject subjectData = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberException(); // 아이디 형식 검사
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberException(); // 비밀번호 재검
        if (!usernameDupCheck(request.getUsername())) throw new CMemberException(); // 아이디 중복 체크

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        String imgurl = gcsService.uploadObject(multipartFile);
        Student student = new Student.Builder(request, subjectData, imgurl).build();
        studentRepository.save(student);

        studentAttendanceService.SetStudentAttendance(student.getId());
    }*/

    // 수강생 복수 R (과정에 해당하지 않는 학생들도 다 불러오는 문제가 있습니다)
    public List<StudentItem> getStudents() {
        List<Student> originData = studentRepository.findAllByOrderByIdAsc();
        List<StudentItem> result = new LinkedList<>();
        for (Student student : originData) result.add(new StudentItem.Builder(student).build());
        return result;
    }

    // 수강생 복수 R 페이징
    public ListResult<StudentItem> getStudentsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Student> studentPage = studentRepository.findAll(pageRequest);
        List<StudentItem> result = new LinkedList<>();
        for (Student student : studentPage) result.add(new StudentItem.Builder(student).build());
        return ListConvertService.settingListResult(result, studentPage.getTotalElements(), studentPage.getTotalPages(), studentPage.getPageable().getPageNumber());
    }

    // 수강생 단수 R ToKen 사용 하여서 보는법
    public StudentResponse getStudent() {
        Student originData = profileService.getStudentData();
        return new StudentResponse.Builder(originData).build();
    }

    /**
     *  수강생 복수 R Token (과정에 해당하는 학생들만 불러오기)
     */
    public List<StudentItem> getStudentsInSubject(long id) {
        Teacher teacherData = profileService.getTeacherData();
        // 과정 id를 가지고 옴
        Subject subject = subjectRepository.findById(id).orElseThrow();
        List<StudentItem> result = new LinkedList<>();
        // 만약 강사의 id와 해당 과정을 진행하는 강사의 id가 같은 경우
        if (teacherData.getId().equals(subject.getTeacher().getId())){
            // 수강생의 id를 통해 수강생의 정보를 가지고 온다
            List<Student> studentData = studentRepository.findBySubjectId(subject.getId());
            for (Student student : studentData) result.add(new StudentItem.Builder(student).build());
            return result;
        } else {
            // 아니면 던진다
            throw new CMemberException();
        }
    }

    /**
     * 수강생 단수 R 강사Token
     */
    public StudentResponse getStudentByTeacher(long studentId) {
        Teacher teacherData = profileService.getTeacherData();
        Student originData = studentRepository.findById(studentId).orElseThrow();
        return new StudentResponse.Builder(originData).build();
    }

    // 수강생 단수 R 관리자용
    public StudentResponse getStudentDetail(long id) {
        Student originData = studentRepository.findById(id).orElseThrow();
        return new StudentResponse.Builder(originData).build();
    }

    // 수강생 U
    public void putStudent(StudentInfoChangeRequest studentInfoChangeRequest) {
        Student originData = profileService.getStudentData();
        originData.putStudentInfo(studentInfoChangeRequest);
        studentRepository.save(originData);
    }

    public void putStudentTest(StudentInfoChangeRequest studentInfoChangeRequest ) {
        long id = 1;
        Student originData = studentRepository.findById(id).orElseThrow();
        originData.putStudentInfo(studentInfoChangeRequest);
        studentRepository.save(originData);
    }

    // 수강생 U 관리자용
    public void putStudentAdmin(StudentInfoChangeAdminRequest studentInfoChangeAdminRequest) {
        Student originData = profileService.getStudentData();
        Subject subjectData = subjectRepository.findById(studentInfoChangeAdminRequest.getSubjectId()).orElseThrow();
        originData.putStudentInfoAdmin(subjectData, studentInfoChangeAdminRequest);
        studentRepository.save(originData);

    }

    // 수강생 U 비밀번호
    public void putStudentPassword(StudentPasswordChangeRequest studentPasswordChangeRequest) {
        Student originData = profileService.getStudentData();
        originData.putStudentPassword(studentPasswordChangeRequest);
        studentRepository.save(originData);
    }

    // 수강생 이미지 U
    public void putStudentImage(MultipartFile multipartFile) throws IOException {
        Student originData = profileService.getStudentData();
        if (multipartFile != null) {
            String imgSrc = gcsService.uploadObject(multipartFile);
            originData.StudentImageUpload(imgSrc);
            studentRepository.save(originData);
        } else {
            originData.StudentImageUpload(null);
            studentRepository.save(originData);
        }
    }

    // 수강생 D
    public void delStudent() {
        studentRepository.deleteById(profileService.getStudentData().getId());
    }

    private boolean usernameDupCheck(String username){
        long dupCount = studentRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
