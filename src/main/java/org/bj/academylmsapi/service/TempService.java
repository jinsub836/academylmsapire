package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.lib.CommonFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class TempService {
    public void setImageByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        System.out.println(file);
        // 멀티파트파일로 이미지 백엔드로 받아오기
    }

        // 그린 이미지를 파일로 만든다
        File newFile = new File("image.jpg"); // 이미지 경로 지정

}
