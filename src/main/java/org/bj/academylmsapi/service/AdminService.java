package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.admin.AdminInfoChangeRequest;
import org.bj.academylmsapi.model.admin.AdminItem;
import org.bj.academylmsapi.model.admin.AdminRequest;
import org.bj.academylmsapi.model.admin.AdminResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.AdminRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 관리자 service

@Service
@RequiredArgsConstructor
public class AdminService {
    private final AdminRepository adminRepository;
    private final PasswordEncoder passwordEncoder;
    private final ProfileService profileService;

    // 관리자 C
    public void createAdmin(AdminRequest request , MemberType memberType) {
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberException(); // 아이디 형식 검사
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberException(); // 비밀번호 재검
        if (!usernameDupCheck(request.getUsername())) throw new CMemberException(); // 아이디 중복 체크

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Admin admin = new Admin.Builder(request, memberType).build();

        adminRepository.save(admin);
    }

    // 관리자 복수 R
    public List<AdminItem> getAdmins() {
        List<Admin> originList = adminRepository.findAllByOrderByIdAsc();
        List<AdminItem> result = new LinkedList<>();
        for (Admin admin : originList) result.add(new AdminItem.Builder(admin).build());

        return result;


    }

    // 관리자 복수 R 페이징
    public ListResult<AdminItem> getAdminsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Admin> adminPage = adminRepository.findAll(pageRequest);
        List<AdminItem> result = new LinkedList<>();
        for (Admin admin : adminPage) result.add(new AdminItem.Builder(admin).build());
        return ListConvertService.settingListResult(result, adminPage.getTotalElements(), adminPage.getTotalPages(), adminPage.getPageable().getPageNumber());
    }

    // 관리자 단수 R Token
    public AdminResponse getAdmin() {
        Admin admin = profileService.getAdminData();
        return new AdminResponse.Builder(admin).build();
    }

    // 관리자 단수 R
    public AdminResponse getAdminDetail(Long id) {
        Admin admin = adminRepository.findById(id).orElseThrow();
        return new AdminResponse.Builder(admin).build();
    }

    // 관리자 U
    public void putAdmin(AdminInfoChangeRequest request) {
        Admin admin = profileService.getAdminData();
        admin.putAdmin(request);
        adminRepository.save(admin);
    }


    // 관리자 D
    public void delAdmin() {
      Admin admin = profileService.getAdminData();
      adminRepository.deleteById(admin.getId());
    }

    // 관리자 아이디 중복 체크
    private boolean usernameDupCheck(String username){
        long dupCount = adminRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
