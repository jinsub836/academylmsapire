package org.bj.academylmsapi.service.trainingManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterItem;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterRequest;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.trainingManagement.TrainingRegisterRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceApprovalService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

// 훈련관리 - 훈련일지 등록 service

@Service
@RequiredArgsConstructor
public class TrainingRegisterService {
    private final TrainingRegisterRepository trainingRegisterRepository;
    private final TeacherRepository teacherRepository;
    private final StudentAttendanceService studentAttendanceService;
    private final ProfileService profileService;
    private final SubjectRepository subjectRepository;
    private final StudentAttendanceApprovalService studentAttendanceApprovalService;
    private final StudentRepository studentRepository;

    // 훈련일지 등록 C
    public void setTrainingRegister(TrainingRegisterRequest request) {
        Teacher teacherData = profileService.getTeacherData();
        Subject subjectData = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        if (trainingRegisterRepository.countByDateRegister(LocalDate.now()) < 1){
        TrainingRegister trainingRegister = new TrainingRegister.Builder(request, teacherData, subjectData).build();

        trainingRegisterRepository.save(trainingRegister);
        studentAttendanceService.putStudentAttendancePlus(request.getSubjectId());

        if(trainingRegister.getAbsentWho() != null){
           String[] studentAbsent = trainingRegister.getAbsentWho().split("@");
            for (String id : studentAbsent){
                long studentNum = studentRepository.countById(Long.parseLong(id));
               if (studentNum > 0){
                   studentAttendanceApprovalService.setStudentAttendanceApprovalById(Long.parseLong(id));
                   studentAttendanceService.putStudentsAbsentPlus(Long.parseLong(id));
               }
            }
        }
        if(trainingRegister.getLateWho() != null){
            String[] studentAbsent = trainingRegister.getLateWho().split("@");
            for (String id : studentAbsent){
                long studentNum = studentRepository.countById(Long.parseLong(id));
                if (studentNum > 0){
                    studentAttendanceApprovalService.setStudentLateApprovalById(Long.parseLong(id));
                    studentAttendanceService.putStudentsLatePlus(Long.parseLong(id));
                }
            }
        }
        if(trainingRegister.getEarlyLeaveWho() != null){
            String[] studentAbsent = trainingRegister.getEarlyLeaveWho().split("@");
            for (String id : studentAbsent){
                long studentNum = studentRepository.countById(Long.parseLong(id));
                if (studentNum > 0){
                    studentAttendanceApprovalService.setStudentEarlyApprovalById(Long.parseLong(id));
                    studentAttendanceService.putStudentsEarlyPlus(Long.parseLong(id));
                }
            }
        }}
    }

    /**
     * 훈련일지 등록 복수 R
     */
    public List<TrainingRegisterItem> getTrainingRegisters(long id) {
        List<TrainingRegister> originData = trainingRegisterRepository.findAllBySubjectId(id);
        List<TrainingRegisterItem> result = new LinkedList<>();
        for (TrainingRegister trainingRegister : originData) result.add(new TrainingRegisterItem.Builder(trainingRegister).build());
        return result;
    }

    /**
     * 훈련일지 등록 복수 R 페이징
     */
    public ListResult<TrainingRegisterItem> getTrainingRegistersPage(int pageNum, long id) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<TrainingRegister> trainingRegisterPage = trainingRegisterRepository.findAllBySubjectId(id, pageRequest);
        List<TrainingRegisterItem> result = new LinkedList<>();
        for (TrainingRegister trainingRegister : trainingRegisterPage) result.add(new TrainingRegisterItem.Builder(trainingRegister).build());
        return ListConvertService.settingListResult(result, trainingRegisterPage.getTotalElements(), trainingRegisterPage.getTotalPages(), trainingRegisterPage.getPageable().getPageNumber());
    }

    // 훈련일지 등록 단수 R
    public TrainingRegisterResponse getTrainingRegister(long id) {
        TrainingRegister originData = trainingRegisterRepository.findById(id).orElseThrow();
        return new TrainingRegisterResponse.Builder(originData).build();
    }

    // 훈련일지 등록 U
    public void putTrainingRegister(long id, TrainingRegisterRequest request) {
        Teacher teacher = profileService.getTeacherData();
        Subject subjectData = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        TrainingRegister originData = trainingRegisterRepository.findById(id).orElseThrow();
        Teacher teacherData = teacherRepository.findById(teacher.getId()).orElseThrow();
        originData.putTrainingRegister(request, teacherData, subjectData);
        trainingRegisterRepository.save(originData);
    }

    // 훈련일지 등록 D
    public void delTrainingRegister(long id) {
        trainingRegisterRepository.deleteById(id);
    }
}
