package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.configure.JwtTokenProvider;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.model.login.LoginRequest;
import org.bj.academylmsapi.model.login.LoginResponse;
import org.bj.academylmsapi.repository.AdminRepository;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final AdminRepository adminRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;


    public LoginResponse doAdminLogin(MemberType memberType , LoginRequest request, String LoginType){
        Admin admin = adminRepository.findByUsername(request.getUsername()+"_admin").orElseThrow(CMemberException::new);

        if (!admin.getMemberType().equals(memberType)) throw new CMemberException();

        if (!passwordEncoder.matches(request.getPassword(), admin.getPassword())) throw new CMemberException();

        String token = jwtTokenProvider.createToken(admin.getUsername(), memberType.getRole() ,LoginType);

        return new  LoginResponse.Builder(token, admin.getAdminName()).build();
    }


    public LoginResponse doTeacherLogin(MemberType memberType , LoginRequest request, String LoginType){
        Teacher teacher = teacherRepository.findByUsername(request.getUsername()+"_teacher").orElseThrow(CMemberException::new);

        if (!teacher.getMemberType().equals(memberType)) throw new CMemberException();

        if (!passwordEncoder.matches(request.getPassword(), teacher.getPassword())) throw new CMemberException();

        String token = jwtTokenProvider.createToken(teacher.getUsername(), memberType.getRole() ,LoginType);

        return new  LoginResponse.Builder(token, teacher.getTeacherName()).build();
    }

    public LoginResponse doStudentLogin(MemberType memberType , LoginRequest request, String LoginType){
        Student student = studentRepository.findByUsername(request.getUsername()).orElseThrow(CMemberException::new);

        if (!student.getMemberType().equals(memberType)) throw new CMemberException();
        System.out.println(passwordEncoder.encode(request.getPassword()));

        if (!passwordEncoder.matches(request.getPassword(), student.getPassword())) throw new CMemberException();

        String token = jwtTokenProvider.createToken(student.getUsername(), memberType.getRole() ,LoginType);

        return new  LoginResponse.Builder(token, student.getStudentName()).build();
    }

}
