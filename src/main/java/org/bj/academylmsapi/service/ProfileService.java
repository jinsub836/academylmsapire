package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.repository.AdminRepository;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final AdminRepository adminRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    public Admin getAdminData(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 일회용 출입증  = 공용 게시판
        String username = authentication.getName();
        return adminRepository.findByUsername(username).orElseThrow();
        //회원 정보 없습니다. 메세지 추가 해야됨 (이유 생각해보기 : 로그인이 안되는 이유는 여러가지 이므로 어떤 이유인지 표시?)
    }

    public Teacher getTeacherData(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 일회용 출입증  = 공용 게시판
        String username = authentication.getName();
        return teacherRepository.findByUsername(username).orElseThrow();
        //회원 정보 없습니다. 메세지 추가 해야됨 (이유 생각해보기 : 로그인이 안되는 이유는 여러가지 이므로 어떤 이유인지 표시?)
    }


    public Student getStudentData(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 일회용 출입증  = 공용 게시판
        String username = authentication.getName();
        return studentRepository.findByUsername(username).orElseThrow();
        //회원 정보 없습니다. 메세지 추가 해야됨 (이유 생각해보기 : 로그인이 안되는 이유는 여러가지 이므로 어떤 이유인지 표시?)
    }
}
