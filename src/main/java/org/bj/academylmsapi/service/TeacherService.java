package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.student.StudentResponse;
import org.bj.academylmsapi.model.teacher.*;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

// 강사 service

@Service
@RequiredArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;
    private final ProfileService profileService;
    private final PasswordEncoder passwordEncoder;
    private final GCSService gcsService;

    // 강사 C
    public void setTeacher(TeacherRequest request) {
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberException(); // 아이디 형식 검사
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberException(); // 비밀번호 재검
        if (!usernameDupCheck(request.getUsername())) throw new CMemberException(); // 아이디 중복 체크

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Teacher teacher = new Teacher.Builder(request).build();

        teacherRepository.save(teacher);
    }

    // 강사 복수 R
    public List<TeacherItem> getTeachers() {
        List<Teacher> originList = teacherRepository.findAll();
        List<TeacherItem> result = new LinkedList<>();
        for (Teacher teacher : originList) result.add(new TeacherItem.Builder(teacher).build());
        return result;
    }


    // 강사 복수 R 페이징
    public ListResult<TeacherItem> getTeachersPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Teacher> teachersPage = teacherRepository.findAll(pageRequest);
        List<TeacherItem> result = new LinkedList<>();
        for (Teacher teacher : teachersPage) result.add(new TeacherItem.Builder(teacher).build());
        return ListConvertService.settingListResult(result, teachersPage.getTotalElements(), teachersPage.getTotalPages(), teachersPage.getPageable().getPageNumber());
    }

    // 강사 단수 R Token
    public TeacherResponse getTeacher() {
        Teacher originData = profileService.getTeacherData();
        return new TeacherResponse.Builder(originData).build();
    }

    // 강사 단수 R
    public TeacherResponse getTeacherDetail(Long id) {
        Teacher originData = teacherRepository.findById(id).orElseThrow();
        return new TeacherResponse.Builder(originData).build();
    }

    // 강사 U
    public void putTeacherInfo(TeacherInfoChangeRequest request) {
        Teacher originData = profileService.getTeacherData();
        originData.putTeacher(request);
        teacherRepository.save(originData);
    }

    // 강사 U 관리자 버전
    public void putTeacherInfo(TeacherInfoChangeAdminRequest request) {
        Teacher originData = profileService.getTeacherData();
        originData.putTeacherAdmin(request);
        teacherRepository.save(originData);
    }

    // 강사 비밀번호 U
    public void putTeacherPassword(TeacherPasswordChangeRequest request) {
        Teacher originData = profileService.getTeacherData();
        originData.putTeacherPassword(request);
        teacherRepository.save(originData);
    }

    /**
     * 강사 이미지 수정
     */
    public void putStudentImage(MultipartFile multipartFile) throws IOException {
        Teacher originData = profileService.getTeacherData();
        if (multipartFile != null) {
            String imgSrc = gcsService.uploadObject(multipartFile);
            originData.putTeacherImage(imgSrc);
            teacherRepository.save(originData);
        } else {
            originData.putTeacherImage(null);
            teacherRepository.save(originData);
        }
    }

        // 강사 D
        public void delTeacher () {
            teacherRepository.deleteById(profileService.getTeacherData().getId());
        }

        private boolean usernameDupCheck (String username){
            long dupCount = teacherRepository.countByUsername(username);
            return dupCount <= 0;
        }
}
