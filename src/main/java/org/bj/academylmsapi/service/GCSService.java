package org.bj.academylmsapi.service;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class GCSService {
    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    public String uploadObject(MultipartFile multipartFile) throws IOException {

        String keyFileName = "upheld-modem-422609-h2-053c8ec6f44a.json";
        InputStream keyFile = ResourceUtils.getURL("classpath:" + keyFileName).openStream();

        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, multipartFile.getOriginalFilename())
                .setContentType(multipartFile.getContentType()).build();

        storage.create(blobInfo, multipartFile.getInputStream());

        return "https://storage.googleapis.com/" + bucketName + "/" + multipartFile.getOriginalFilename(); // 필요한건 업로드 한 다음에 gcs에 쓸 경로
    }

}