package org.bj.academylmsapi.service.testManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.model.testManagement.subjectStatus.SubjectStatusChangeRequest;
import org.bj.academylmsapi.model.testManagement.subjectStatus.SubjectStatusItem;
import org.bj.academylmsapi.model.testManagement.subjectStatus.SubjectStatusRequest;
import org.bj.academylmsapi.model.testManagement.subjectStatus.SubjectStatusResponse;
import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.testManagement.SubjectStatusRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 평가관리 - 과목현황 service

@Service
@RequiredArgsConstructor
public class SubjectStatusService {
    private final SubjectStatusRepository subjectStatusRepository;
    private final TeacherRepository teacherRepository;
    private final SubjectRepository subjectRepository;
    private final ProfileService profileService;


    // 과목현황 C
    public void setSubjectStatus(SubjectStatusRequest request){
        Teacher teacher = profileService.getTeacherData();
        Subject subject = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        subjectStatusRepository.save(new SubjectStatus.Builder(subject,teacher,request).build());
    }

    // 과목현황 복수 R
    public List<SubjectStatusItem> getSubjectStatusItem(){
        List<SubjectStatus> originData = subjectStatusRepository.findAll();
        List<SubjectStatusItem> result = new LinkedList<>();
        for (SubjectStatus subjectStatus : originData ) result.add(new SubjectStatusItem.Builder(subjectStatus).build());
        return result;
    }


    // 과목현황 복수 R
    public List<SubjectStatusItem> getSubjectStatusItem(long subjectId,int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<SubjectStatus> subjectStatusPage = subjectStatusRepository.findAllBySubjectIdIdOrderByDateRegister(subjectId,pageRequest);
        List<SubjectStatusItem> result = new LinkedList<>();
        for (SubjectStatus subjectStatus : subjectStatusPage ) result.add(new SubjectStatusItem.Builder(subjectStatus).build());
        return result;
    }

    // 과목현황 단수 R
    public SubjectStatusResponse getSubjectStatusResponse(long id){
        SubjectStatus subjectStatus = subjectStatusRepository.findById(id).orElseThrow();
        return new SubjectStatusResponse.Builder(subjectStatus).build();
    }

    // 과목현황 U
    public void putSubjectStatus(long id, SubjectStatusChangeRequest request){
        Teacher teacher = teacherRepository.findById(request.getSubjectId()).orElseThrow();
        Subject subject = subjectRepository.findById(request.getSubjectId()).orElseThrow();
        SubjectStatus subjectStatus = subjectStatusRepository.findById(id).orElseThrow();
        subjectStatus.putSubjectStatus(subject,teacher,request);
        subjectStatusRepository.save(subjectStatus);
    }

    // 과목현황 D
    public void delSubjectStatus(long id){
        subjectStatusRepository.deleteById(id);
    }
}
