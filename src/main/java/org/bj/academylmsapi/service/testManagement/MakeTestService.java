package org.bj.academylmsapi.service.testManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.testManagement.makeTest.MakeTestRequest;
import org.bj.academylmsapi.model.testManagement.makeTest.MakeTestResponse;
import org.bj.academylmsapi.repository.testManagement.MakeTestRepository;
import org.bj.academylmsapi.repository.testManagement.SubjectStatusRepository;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.stereotype.Service;

// 평가관리 - 시험출제 service

@Service
@RequiredArgsConstructor
public class MakeTestService {
    private final MakeTestRepository makeTestRepository;
    private final SubjectStatusRepository subjectStatusRepository;


    public CommonResult setMakeTest(MakeTestRequest request) {
        System.out.println(makeTestRepository.countBySubjectStatusId(request.getSubjectStatusId()));
        if (makeTestRepository.countBySubjectStatusId(request.getSubjectStatusId())<1){
        SubjectStatus subjectStatus = subjectStatusRepository.findById(request.getSubjectStatusId()).orElseThrow();
        subjectStatus.putSubjectStatusChange();
        makeTestRepository.save(new MakeTest.Builder(subjectStatus, request).build());
        subjectStatusRepository.save(subjectStatus);
        return ResponseService.getSuccessResult();
        }
        else {
            return ResponseService.getFailResult(ResultCode.FAILURE);
        }
    }

    public MakeTestResponse getMakeTests(long id) {
        SubjectStatus subjectStatus = subjectStatusRepository.findById(id).orElseThrow();
        MakeTest makeTest = makeTestRepository.findBySubjectStatusId(subjectStatus.getId());
        return new MakeTestResponse.Builder(makeTest).build();
    }

    public void putMakeTest(MakeTestRequest request, long id) {
        SubjectStatus subjectStatus = subjectStatusRepository.findById(request.getSubjectStatusId()).orElseThrow();
        MakeTest OriginData = makeTestRepository.findById(id).orElseThrow();
        OriginData.putMakeTest(subjectStatus, request);
        makeTestRepository.save(OriginData);
    }

    public void delMakeTest(long id) {
        makeTestRepository.deleteById(id);
    }
}
