package org.bj.academylmsapi.service.testManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.testManagement.answerTest.*;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.testManagement.AnswerTestRepository;
import org.bj.academylmsapi.repository.testManagement.MakeTestRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 평가관리 - 시험출제 service

@Service
@RequiredArgsConstructor
public class AnswerTestService {
    private final AnswerTestRepository answerTestRepository;
    private final StudentRepository studentRepository;
    private final MakeTestRepository makeTestRepository;
    private final ProfileService profileService;

    public void setAnswer(AnswerTestRequest request) {
        MakeTest makeTest = makeTestRepository.findById(request.getMakeTest()).orElseThrow();
        Student student = profileService.getStudentData();
        AnswerTest answerTest = new AnswerTest.Builder(makeTest, student, request).build();
        answerTestRepository.save(answerTest);
    }

    /**
     * 시험채점 리스트 보기
     */
    public List<AnswerTestItem> getAnswers(long id) {
        MakeTest makeTest = makeTestRepository.findBySubjectStatusId(id);
        List<AnswerTest> origin = answerTestRepository.findAllByMakeTestIdOrderByStudentId(makeTest.getId());
        List<AnswerTestItem> result = new LinkedList<>();
        for (AnswerTest answerTest : origin) result.add(new AnswerTestItem.Builder(answerTest).build());
        return result;
    }

    /**
     * 시험채점 리스트 보기 페이징
     */
    public ListResult<AnswerTestItem> getAnswersPage(long id,int pageNum) {
        MakeTest makeTest = makeTestRepository.findBySubjectStatusId(id);
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<AnswerTest> answerTestPage = answerTestRepository.findAllByMakeTestIdOrderByStudentId(makeTest.getId(), pageRequest);
        List<AnswerTestItem> result = new LinkedList<>();
        for (AnswerTest answerTest : answerTestPage) result.add(new AnswerTestItem.Builder(answerTest).build());
        return ListConvertService.settingListResult(result, answerTestPage.getTotalElements(), answerTestPage.getTotalPages(), answerTestPage.getPageable().getPageNumber());
    }

    public SingleResult<AnswerTestResponse> getAnswer(long id) {
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        AnswerTestResponse answerTestResponse = new AnswerTestResponse.Builder(answerTest).build();

        return ResponseService.getSingleResult(answerTestResponse);
    }

    public CommonResult putAnswerScore(AnswerPutScoreRequest request, long id) {
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        answerTest.getScore(request);
        answerTestRepository.save(answerTest);
        return ResponseService.getSuccessResult();
    }

    public CommonResult putTestCheckSign(AnswerPutStudentSignRequest request, long id) {
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        answerTest.AnswerTestSign(request);
        answerTestRepository.save(answerTest);

        return ResponseService.getSuccessResult();
    }


    public CommonResult delAnswer(long id) {
        answerTestRepository.findById(id);
        return ResponseService.getSuccessResult();
    }
}
