package org.bj.academylmsapi.service.board;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.board.board.BoardItem;
import org.bj.academylmsapi.model.board.board.BoardRequest;
import org.bj.academylmsapi.model.board.board.BoardResponse;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.board.BoardRepository;
import org.bj.academylmsapi.repository.board.ReplyRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

// 게시판 - 게시판 service

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final ReplyRepository replyRepository;
    private final ProfileService profileService;
    // 게시판 C
    public void setBoard(BoardRequest request) {
        boardRepository.save(new Board.Builder(request).build());
    }

    // 게시판 복수 R
    public List<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAll();
        List<BoardItem> result = new LinkedList<>();
        for (Board board : originList)
            result.add(new BoardItem.Builder(board).build());
        return result;
    }

    // 게시판 복수 R 페이징
    public ListResult<BoardItem> getBoardsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boardPage = boardRepository.findAll(pageRequest);
        List<BoardItem> result = new LinkedList<>();
        for (Board board : boardPage) result.add(new BoardItem.Builder(board).build());
        return ListConvertService.settingListResult(result, boardPage.getTotalElements(), boardPage.getTotalPages(), boardPage.getPageable().getPageNumber());
    }

    // 게시판 단수 R
    public BoardResponse getBoard(long id) {
        Board originData = boardRepository.findById(id).orElseThrow();
        Optional<Reply> replyData = replyRepository.findById(id);
        return new BoardResponse.Builder(originData).comment(replyData.orElse(null)).build();
    }

    // 게시판 U
    public void putBoard(long id, BoardRequest request) {
        Board originData = boardRepository.findById(id).orElseThrow();
        originData.putBoard(request);
        boardRepository.save(originData);
    }

    // 게시판 D
    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
