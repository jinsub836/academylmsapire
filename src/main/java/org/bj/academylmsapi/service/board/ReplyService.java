package org.bj.academylmsapi.service.board;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.board.reply.ReplyItem;
import org.bj.academylmsapi.model.board.reply.ReplyRequest;
import org.bj.academylmsapi.model.board.reply.ReplyResponse;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.board.BoardRepository;
import org.bj.academylmsapi.repository.board.ReplyRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 게시판 - 답변 service

@Service
@RequiredArgsConstructor
public class ReplyService {
    private final ReplyRepository replyRepository;
    private final BoardRepository boardRepository;
    private final ProfileService profileService;

    // 답변 C
    public void setReply(ReplyRequest request) {
        Board boardData = boardRepository.findById(request.getBoardId()).orElseThrow();
        replyRepository.save(new Reply.Builder(request, boardData).build());
    }

    // 답변 복수 R
    public List<ReplyItem> getReplys() {
        List<Reply> originList = replyRepository.findAll();
        List<ReplyItem> result = new LinkedList<>();
        for (Reply reply : originList) result.add(new ReplyItem.Builder(reply).build());
        return result;
    }

    // 답변 복수 R 페이징
    public ListResult<ReplyItem> getReplysPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Reply> replyPage = replyRepository.findAll(pageRequest);
        List<ReplyItem> result = new LinkedList<>();
        for (Reply reply : replyPage) result.add(new ReplyItem.Builder(reply).build());
        return ListConvertService.settingListResult(result, replyPage.getTotalElements(), replyPage.getTotalPages(), replyPage.getPageable().getPageNumber());
    }

    // 답변 단수 R
    public ReplyResponse getReply(long id) {
        Reply originData = replyRepository.findById(id).orElseThrow();
        return new ReplyResponse.Builder(originData).build();
    }

    // 답변 단수 R Token 수강생
    public ReplyResponse getReplyTokenStudent() {
        Student studentData = profileService.getStudentData();
        Reply originData = replyRepository.findById(studentData.getId()).orElseThrow();
        return new ReplyResponse.Builder(originData).build();
    }

    // 답변 단수 R Token 강사
    public ReplyResponse getReplyTokenTeacher() {
        Teacher teacherData = profileService.getTeacherData();
        Reply originData = replyRepository.findById(teacherData.getId()).orElseThrow();
        return new ReplyResponse.Builder(originData).build();
    }

    // 답변 U
    public void putReply(long id, ReplyRequest request) {
        Reply originData = replyRepository.findById(id).orElseThrow();
        originData.putReply(request);
        replyRepository.save(originData);
    }

    // 답변 D
    public void delReply(long id) {
        replyRepository.deleteById(id);
    }
}
