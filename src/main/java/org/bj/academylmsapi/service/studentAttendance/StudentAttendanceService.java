package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceItem;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceResponse;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceApprovalRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceRepository;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

//  수강생 출결 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceService {
    private final StudentAttendanceRepository studentAttendanceRepository;
    private final StudentRepository studentRepository;
    private final ProfileService profileService;

    // 수강생 출결 C
    public void SetStudentAttendance(Long id) {
        Student studentData = studentRepository.findById(id).orElseThrow();
        studentAttendanceRepository.save(new StudentAttendance.Builder(studentData).build());
    }

    // 수강생 단수 R App 연동용
    public StudentAttendanceResponse getStudentAttendancesRate(){
        Student student = profileService.getStudentData();
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(student.getId());
        return new StudentAttendanceResponse.Builder(studentAttendance).build();
    }
    // 수강생 단수 R Web 연동용
    public StudentAttendanceResponse getStudentAttendanceAdmin(Long id){
        Student student = studentRepository.findById(id).orElseThrow();
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(student.getId());
        return new StudentAttendanceResponse.Builder(studentAttendance).build();
    }


    // 수강생 출결 복수 R
    public List<StudentAttendanceItem> getStudentAttendances() {
        List<StudentAttendance> originList = studentAttendanceRepository.findAll();
        List<StudentAttendanceItem> result = new LinkedList<>();
        for (StudentAttendance studentAttendance : originList)
            result.add(new StudentAttendanceItem.Builder(studentAttendance).build());
        return result;
    }

    // 수강생 출결 복수 R 페이징
    public ListResult<StudentAttendanceItem> getStudentAttendancesPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<StudentAttendance> studentAttendancesPage = studentAttendanceRepository.findAll(pageRequest);
        List<StudentAttendanceItem> result = new LinkedList<>();
        for (StudentAttendance studentAttendance : studentAttendancesPage)
            result.add(new StudentAttendanceItem.Builder(studentAttendance).build());
        return ListConvertService.settingListResult(result, studentAttendancesPage.getTotalElements(), studentAttendancesPage.getTotalPages(), studentAttendancesPage.getPageable().getPageNumber());
    }

    // 수강생 U
    public void putStudentAttendance(long id, StudentAttendanceRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        StudentAttendance originData = studentAttendanceRepository.findById(id).orElseThrow();
        originData.putStudentAttendance(request, studentData);
        studentAttendanceRepository.save(originData);
    }

    public void putStudentAttendancePlus(Long id) {
        // 수강 과목 아이디로 해당 수업 학생 정보 다 불러옴
        List<Student> originData = studentRepository.findBySubjectId(id);
        for (Student student : originData) {
            // 찾아온 학생 수 만큼 반복
            StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(student.getId());
            // 찾아온 학생 아이디로 찾아서
            studentAttendance.putStudentAttendanceByTrainReport(studentAttendance);
            //출석 1 올려줌
            studentAttendanceRepository.save(studentAttendance);
            //변경 내용 저장
        }
    }

    // 결석 1 추가!!!
    public void putStudentsAbsentPlus(Long id) {
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(id);
        studentAttendance.putStudentAttendanceByAbsent(studentAttendance);
        studentAttendanceRepository.save(studentAttendance);
    }

    // 지각 1 추가
    public void putStudentsLatePlus(Long id) {
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(id);
        studentAttendance.putStudentAttendanceByLate(studentAttendance);
        studentAttendanceRepository.save(studentAttendance);
    }

    // 조퇴 1 추가
    public void putStudentsEarlyPlus(Long id) {
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(id);
        studentAttendance.putStudentAttendanceByEarly(studentAttendance);
        studentAttendanceRepository.save(studentAttendance);
    }

    //출결 승인 후 동작
    public void putAdminApproval(long id, AttendanceType state){
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(id);
        studentAttendance.putStudentAttendanceApproval(studentAttendance , state);
        studentAttendanceRepository.save(studentAttendance);
    }


    // 수강생 D
    public void delStudentAttendance(long id) {
        studentAttendanceRepository.deleteById(id);
    }
}
