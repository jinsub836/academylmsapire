package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.enums.Month;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.*;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceApprovalRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceRepository;
import org.bj.academylmsapi.service.GCSService;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;

import java.util.LinkedList;
import java.util.List;


// 수강생 출결 승인 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceApprovalService {
    private final StudentAttendanceApprovalRepository studentAttendanceApprovalRepository;
    private final StudentRepository studentRepository;
    private final StudentAttendanceRepository studentAttendanceRepository;
    private final ProfileService profileService;
    private final StudentAttendanceService studentAttendanceService;
    private final GCSService gcsService;

    /**
     * 수강생 출결 승인 C
     */
    public void setStudentAttendanceApproval(StudentAttendanceApprovalRequest request){
        Student studentData = profileService.getStudentData();
        long checkDuplicateResisterDate = studentAttendanceApprovalRepository.countByDateRegister(LocalDate.now());
        if (checkDuplicateResisterDate < 100) setStudentAttendanceFactory(request,studentData);
        setStudentAttendanceApprovalState(request);
    }


    public void setStudentAttendanceApprovalById(Long id) {
        autoSetter(id,"금일 결석",AttendanceType.ABSENT);
    }
    public void setStudentLateApprovalById(Long id) {
       autoSetter(id,"금일 지각",AttendanceType.LATE);
    }
    public void setStudentEarlyApprovalById(Long id) {
        autoSetter(id,"금일 조퇴",AttendanceType.EARLYLEAVE);
    }

    //수강생 출결 요청 리스트 확인
    public List<StudentAttendanceApprovalMonthly> getMyAttendanceApprovals(){
        long id = profileService.getStudentData().getId();
        List<StudentAttendanceApproval> originData = studentAttendanceApprovalRepository.findAllByStudentId(id);
        List<StudentAttendanceApprovalMonthly> result = new LinkedList<>();
        for(StudentAttendanceApproval attendanceApproval : originData)
            result.add(new StudentAttendanceApprovalMonthly.Builder(attendanceApproval).build());
        return result;
    }

    //수강생 출결 요청 리스트 확인 (페이징)
    public ListResult<StudentAttendanceApprovalMonthly> getMyAttendanceApprovals(int pageNum){
        long id = profileService.getStudentData().getId();
        PageRequest pageRequest = ListConvertService.getPageable2(pageNum);
        Page<StudentAttendanceApproval> originDataPage = studentAttendanceApprovalRepository.findAllByStudentId(id,pageRequest);
        List<StudentAttendanceApprovalMonthly> result = new LinkedList<>();
        for(StudentAttendanceApproval attendanceApproval : originDataPage)
            result.add(new StudentAttendanceApprovalMonthly.Builder(attendanceApproval).build());
        return ListConvertService.settingListResult(result, originDataPage.getTotalElements(),
                originDataPage.getTotalPages(), originDataPage.getPageable().getPageNumber());
    }

    // 수강생 본인 출결 요청 리스트 확인 (월별)
    public List<StudentAttendanceApprovalMonthly> getMyAttendanceApprovalsMonthly(String date){
        long id = profileService.getStudentData().getId();
        List<StudentAttendanceApproval> originData = studentAttendanceApprovalRepository.findAllByStudentId(id);
        List<StudentAttendanceApprovalMonthly> result = new LinkedList<>();
        for(StudentAttendanceApproval attendanceApproval : originData)
           if(attendanceApproval.getDateEnd().toString().substring(5,7).equals(date))
           {result.add(new StudentAttendanceApprovalMonthly.Builder(attendanceApproval).build());}
        return result;
    }

    // 수강생 본인 출결 요청 리스트 확인 (날짜 범위)
    public List<StudentAttendanceApprovalResponse> getMyAttendanceApprovalOfDate(String dateStart , String dateEnd){
        LocalDate date1 = LocalDate.parse(dateStart);
        long id = profileService.getStudentData().getId();
        List<StudentAttendanceApproval> originData;
        if(dateEnd == null) originData =studentAttendanceApprovalRepository.findAllByStudentIdAndDateRegisterOrderByDateRegister(id, date1);
        else {
            LocalDate date2 = LocalDate.parse(dateEnd);
            originData = studentAttendanceApprovalRepository.findAllByStudentIdAndDateRegisterBetweenOrderByDateRegister(id, date1, date2 );
        }
        List<StudentAttendanceApprovalResponse> responses = new LinkedList<>();
        for (StudentAttendanceApproval attendanceApproval : originData)
            responses.add(new StudentAttendanceApprovalResponse.Builder(attendanceApproval).build());
        return responses;
        }


    // 수강생 출결 승인 단수 R
    public SingleResult<StudentAttendanceApprovalResponse> getStudentAttendanceApprovalDetail(long id){
        StudentAttendanceApproval student = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        StudentAttendanceApprovalResponse studentAttendanceApprovalResponse = new StudentAttendanceApprovalResponse.Builder(student).build();
        return ResponseService.getSingleResult(studentAttendanceApprovalResponse);
    }

    // 수강생 출결 승인 복수 R
    public List<StudentAttendanceApprovalItem> getStudentAttendanceApprovals(long state) {
        List<StudentAttendanceApproval> originList = studentAttendanceApprovalRepository.findAllByOrderByDateRegisterDesc();
        List<StudentAttendanceApprovalItem> result = new LinkedList<>();
        if (state == 1){
        for (StudentAttendanceApproval studentAttendanceApproval : originList) {
                result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());}}
        else if (state == 2) {
            for (StudentAttendanceApproval studentAttendanceApproval : originList) {
                if (studentAttendanceApproval.getApprovalState().equals(ApprovalState.NOTAPPROVED))
                  result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());}
        }else {
            for (StudentAttendanceApproval studentAttendanceApproval : originList) {
                if (studentAttendanceApproval.getApprovalState().equals(ApprovalState.ADMINAPPROVAL))
                    result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());}
        }
        return result;
    }

    // 수강생 출결 승인 복수 R 페이징
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovalsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<StudentAttendanceApproval> studentAttendanceApprovalPage = studentAttendanceApprovalRepository.findAll(pageRequest);
        List<StudentAttendanceApprovalItem> result = new LinkedList<>();
        for (StudentAttendanceApproval studentAttendanceApproval : studentAttendanceApprovalPage)
            result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());

        return ListConvertService.settingListResult(result);
    }
    // 수강생 출결 승인 U
    public void putStudentAttendanceApproval(long id, StudentAttendanceApprovalFixRequest request) {
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApproval(request);
        studentAttendanceApprovalRepository.save(originData);
    }

    /**
     * 수강생 출결 승인 수정 학생버전
     */
    public void putStudentAttendanceApprovalChangeInfoByStudent(long id,StudentAttendanceApprovalChangeInfoByStudentRequest request) {
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApprovalChangeInfoByStudentRequest(request);
        studentAttendanceApprovalRepository.save(originData);
    }

    public void putStudentAttendanceApprovalChangeAddFile(MultipartFile file, long id) throws IOException {
        StudentAttendanceApproval studentAttendanceApproval = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        if (file != null) {
            String imgSrc = gcsService.uploadObject(file);
            studentAttendanceApproval.putStudentAttendanceApprovalChangeAddFile(imgSrc);
            studentAttendanceApprovalRepository.save(studentAttendanceApproval);
        } else {
            studentAttendanceApproval.putStudentAttendanceApprovalChangeAddFile(null);
            studentAttendanceApprovalRepository.save(studentAttendanceApproval);
        }
    }

    // 관리자 승인
    public void putStudentAttendanceApprovalAdmin(long id){
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApprovalAdmin();
        studentAttendanceApprovalRepository.save(originData);

        studentAttendanceService.putAdminApproval(originData.getStudent().getId(),originData.getAttendanceType());
    }
    // 수강생 출결승인 D
    public void delStudentAttendanceApproval(long id) {
        studentAttendanceApprovalRepository.deleteById(id);
    }

    private void autoSetter(long id , String setReason , AttendanceType attendanceType){
        Student studentData = studentRepository.findAllById(id).orElseThrow();
        StudentAttendanceApproval attendanceApproval = new StudentAttendanceApproval();
        attendanceApproval.setStudent(studentData);
        attendanceApproval.setReason(setReason);
        attendanceApproval.setDateStart(LocalDate.now());
        attendanceApproval.setDateEnd(LocalDate.now());
        attendanceApproval.setAttendanceType(attendanceType);
        attendanceApproval.setDateRegister(LocalDate.now());
        attendanceApproval.setApprovalState(ApprovalState.NOTAPPROVED);
        studentAttendanceApprovalRepository.save(attendanceApproval);
    }

    private void setStudentAttendanceFactory(StudentAttendanceApprovalRequest request , Student student){
        StudentAttendanceApproval studentAttendanceApproval = new StudentAttendanceApproval.Builder(request , student).build();
        StudentAttendance studentAttendance = studentAttendanceRepository.findByStudentId(student.getId());
        studentAttendanceApprovalRepository.save(studentAttendanceApproval);
        if (studentAttendanceApproval.getAttendanceType().equals(AttendanceType.ABSENT))
            studentAttendanceService.putStudentsAbsentPlus(studentAttendance.getId());
        else if (studentAttendanceApproval.getAttendanceType().equals(AttendanceType.EARLYLEAVE)) {
            studentAttendanceService.putStudentsEarlyPlus(studentAttendance.getId());
        } else studentAttendanceService.putStudentsLatePlus(studentAttendance.getId());
    }

    private void setStudentAttendanceApprovalState(StudentAttendanceApprovalRequest request){
        Student studentData = profileService.getStudentData();
        if (request.getAttendanceType().equals("ABSENT")) studentAttendanceService.putStudentsAbsentPlus(studentData.getId());
        else if(request.getAttendanceType().equals("EARLYLEAVE")) studentAttendanceService.putStudentsEarlyPlus(studentData.getId());
        else if(request.getAttendanceType().equals("LATE")) studentAttendanceService.putStudentsLatePlus(studentData.getId());
    }
}
