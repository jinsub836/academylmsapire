package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.repository.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class UploadService {
    private final GCSService gcsService;
    private final StudentRepository studentRepository;
    private final ProfileService profileService;

    public void uploadImage(MultipartFile file) throws IOException {
        String imgurl = gcsService.uploadObject(file);

        Student studentData = profileService.getStudentData();
        studentData.StudentImageUpload(imgurl);
        studentRepository.save(studentData);

    }
}
