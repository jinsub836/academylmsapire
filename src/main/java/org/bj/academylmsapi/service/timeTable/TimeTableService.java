package org.bj.academylmsapi.service.timeTable;

import lombok.RequiredArgsConstructor;

import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.timeTable.TimeTable;
import org.bj.academylmsapi.lib.CommonFile;
import org.bj.academylmsapi.model.timeTable.TimeTableItem;

import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.repository.TimeTableRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class TimeTableService {
    private final SubjectRepository subjectRepository;
    private final TimeTableRepository timeTableRepository;


    public void setTimeTable(MultipartFile multipartFile, Long id) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        Subject subject = subjectRepository.findById(id).orElseThrow();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        TimeTable timeTable = new TimeTable();

        int index = 0;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                if (index > 0) {
                    String[] cols = line.split(",");
                    if (cols.length == 6 && cols[5].equals("2")){
                        timeTable.setSubject(subject);
                        timeTable.setDateTraining(cols[0]);
                        timeTable.setStartTimeTrain(cols[1]);
                        timeTable.setEndTime(cols[2]);
                        timeTable.setIsRemote(cols[3]);
                        timeTable.setStartTime(cols[4]);
                        timeTable.setTimeSorted(cols[5]);
                        timeTable.setPlaceCode("");
                        timeTable.setSubjectCode("");
                        timeTable.setTeacherCode("");
                        System.out.println(index);
                        System.out.println(line);
                        System.out.println("----------------------------------");
                        timeTableRepository.save(timeTable);}
                    else if (cols.length == 9){
                        timeTable = new TimeTable.Builder(cols,subject).build();
                        System.out.println(index);
                        System.out.println(line);
                        System.out.println("----------------------------------");
                        timeTableRepository.save(timeTable);}
                }
                index++;
            }
        } catch (Exception e) {
            System.out.println("동작 멈춤");
        }
        bufferedReader.close();
    }
    //시간표 불러오기 (지정한 날짜를 통해) 5일을 불러오는데 리스트로 담아주며
    // 시작 시간
    public List<TimeTableItem> getTimeTable(){
        List<TimeTable> originData = timeTableRepository.findAll();
        List<TimeTableItem> result = new LinkedList<>();
        for (TimeTable timeTable : originData) result.add(new TimeTableItem.Builder(timeTable).build());
        return result;
    }
}
