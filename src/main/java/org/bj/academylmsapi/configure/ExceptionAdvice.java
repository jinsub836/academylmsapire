package org.bj.academylmsapi.configure;

import jakarta.servlet.http.HttpServletRequest;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILURE);
    }

    @ExceptionHandler(CAcademyFullException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAcademyFullException e) {
        return ResponseService.getFailResult(ResultCode.FAILURE);
    }
}
