package org.bj.academylmsapi.repository.testManagement;

import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.model.testManagement.subjectStatus.SubjectStatusItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

// 평가관리 - 과목현황

public interface SubjectStatusRepository extends JpaRepository<SubjectStatus, Long> {
    Page<SubjectStatus> findAllBySubjectIdIdOrderByDateRegister(Long id, Pageable pageable);
}
