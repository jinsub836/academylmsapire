package org.bj.academylmsapi.repository.testManagement;

import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 평가관리 - 시험채점

public interface AnswerTestRepository extends JpaRepository<AnswerTest, Long> {
    Page<AnswerTest> findAllByMakeTestIdOrderByStudentId(long id, Pageable pageable);
    List<AnswerTest> findAllByMakeTestIdOrderByStudentId(long id);
}
