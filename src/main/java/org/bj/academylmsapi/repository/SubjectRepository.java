package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 과정 repository

public interface SubjectRepository extends JpaRepository<Subject, Long> {
   List<Subject> findAllByTeacherId(long id);
   Page<Subject> findAllByTeacherId(long id, Pageable pageable);
   Subject findByTeacherId(long id);
}
