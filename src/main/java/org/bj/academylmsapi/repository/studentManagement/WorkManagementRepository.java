package org.bj.academylmsapi.repository.studentManagement;

import org.bj.academylmsapi.entity.studentManagement.WorkManagement;
import org.springframework.data.jpa.repository.JpaRepository;

// 학생관리 - 취업관리 - 취업 관리사항

public interface WorkManagementRepository extends JpaRepository<WorkManagement, Long> {
}
