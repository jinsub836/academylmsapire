package org.bj.academylmsapi.repository.studentAttendance;

import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

// 수강생 출결 승인 repository

public interface StudentAttendanceApprovalRepository extends JpaRepository<StudentAttendanceApproval, Long> {
    List<StudentAttendanceApproval> findAllByOrderByDateRegisterDesc();
    List<StudentAttendanceApproval> findAllByStudentId(Long id);
    List<StudentAttendanceApproval> findAllByStudentIdAndDateRegisterOrderByDateRegister(Long id , LocalDate dateStart);
    List<StudentAttendanceApproval> findAllByStudentIdAndDateRegisterBetweenOrderByDateRegister(Long id ,LocalDate dateStart,LocalDate dateEnd);
    Page<StudentAttendanceApproval> findAllByStudentId(Long id, Pageable pageable);

    long countByDateRegister(LocalDate localDate);
}
