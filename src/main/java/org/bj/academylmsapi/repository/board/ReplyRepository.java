package org.bj.academylmsapi.repository.board;

import org.bj.academylmsapi.entity.board.Reply;
import org.springframework.data.jpa.repository.JpaRepository;

// 게시판 - 게시판답변 repository

public interface ReplyRepository extends JpaRepository<Reply, Long> {
}
