package org.bj.academylmsapi.repository.trainingManagement;

import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.Pageable;
import java.time.LocalDate;
import java.util.List;

// 훈련관리 - 훈련일지 등록

public interface TrainingRegisterRepository extends JpaRepository<TrainingRegister, Long> {

    List<TrainingRegister> findAllBySubjectId(Long subjectId);
    Page<TrainingRegister> findAllBySubjectId(Long subjectId, PageRequest pageRequest);

    long countByDateRegister(LocalDate date);
}
