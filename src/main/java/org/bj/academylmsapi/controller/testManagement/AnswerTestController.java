package org.bj.academylmsapi.controller.testManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.testManagement.answerTest.*;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.testManagement.AnswerTestService;
import org.springframework.web.bind.annotation.*;

// 평가관리 - 시험채점 controller

@RestController
@RequiredArgsConstructor
@Tag(name = "AnswerTest" , description = "[평가관리] 시험 답변")
@RequestMapping("/v1/answer-test")
public class AnswerTestController {
    private final AnswerTestService answerTestService;

    @PostMapping("/new")
    @Operation(summary = "시험등록")
    public CommonResult setAnswer(@RequestBody @Valid AnswerTestRequest request){
        answerTestService.setAnswer(request);
        return ResponseService.getSuccessResult();
    }

    /**
     * 시험채점 리스트 보기
     */
    @GetMapping("/all/{subjectStatusId}")
    @Operation(summary = "시험채점 리스트 보기")
    public ListResult<AnswerTestItem> getAnswers(@PathVariable long subjectStatusId) {
        return ListConvertService.settingListResult(answerTestService.getAnswers(subjectStatusId));
    }

    /**
     * 시험채점 리스트 보기 페이징
     */
    @GetMapping("/all/{subjectStatusId}/{pageNum}")
    @Operation(summary = "시험채점 리스트 페이징 / 이것 말고 위에거")
    public ListResult<AnswerTestItem> getAnswersPage(@PathVariable long subjectStatusId , @PathVariable int pageNum) {
        return answerTestService.getAnswersPage(subjectStatusId,pageNum);
    }

    @GetMapping("/detail/answerTestId/{answerTestId}")
    @Operation(summary = "답변 보기")
    public SingleResult<AnswerTestResponse> getAnswer(@PathVariable long answerTestId){
        return answerTestService.getAnswer(answerTestId);
    }

    @PutMapping("/putScore/answerTestId/{answerTestId}")
    @Operation(summary = "채점")
    public CommonResult putAnswerScore(@PathVariable long answerTestId, @RequestBody @Valid AnswerPutScoreRequest request){
        return answerTestService.putAnswerScore(request, answerTestId);
    }

    @PutMapping("/testSign/answerTestId/{answerTestId}")
    @Operation(summary = "확인 서명")
    public CommonResult putTestCheckSign(@PathVariable long answerTestId, @RequestBody @Valid AnswerPutStudentSignRequest request){
        return answerTestService.putTestCheckSign(request, answerTestId);
    }

    @DeleteMapping("delete/answerTestId/{answerTestId}")
    @Operation(summary = "답변 삭제")
    public CommonResult delAnswer(@PathVariable long answerTestId){
        return answerTestService.delAnswer(answerTestId);
    }
}
