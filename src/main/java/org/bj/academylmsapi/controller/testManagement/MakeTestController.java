package org.bj.academylmsapi.controller.testManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.testManagement.makeTest.MakeTestRequest;
import org.bj.academylmsapi.model.testManagement.makeTest.MakeTestResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.testManagement.MakeTestService;
import org.springframework.web.bind.annotation.*;

// 평가관리 - 시험출제 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/makeTest")
@Tag(name = "MakeTest" , description = "[평가관리] 시험출제")
public class MakeTestController {
    private final MakeTestService makeTestService;

    @PostMapping("/new")
    @Operation(summary = "시험 출제")
    public CommonResult setMakeTest(@RequestBody @Valid MakeTestRequest request){
        return  makeTestService.setMakeTest(request);
    }

    @GetMapping("/detail/subjectStatusId/{subjectStatusId}")
    @Operation(summary = "시험 상세 보기")
    public SingleResult<MakeTestResponse> getMakeTests(@PathVariable long subjectStatusId){
        return ResponseService.getSingleResult(makeTestService.getMakeTests(subjectStatusId));
    }

    @PutMapping("/change/makeTestId/{makeTestId}")
    @Operation(summary = "시험 출제 정보 수정")
    public CommonResult putMakeTest (@RequestBody @Valid MakeTestRequest request,@PathVariable long makeTestId ){
        makeTestService.putMakeTest(request, makeTestId);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/makeTestId/{makeTestId}")
    @Operation(summary = "시험 삭제")
    public CommonResult delMakeTest (@PathVariable long makeTestId){
        makeTestService.delMakeTest(makeTestId);
        return ResponseService.getSuccessResult();
    }
}
