package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.*;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceApprovalService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

// 수강생 출결 승인 controller
@RestController
@RequiredArgsConstructor
@RequestMapping("v1/studentapproval")
@Tag(name = "StudentAttendanceApproval", description = "수강생 출결 승인")
public class StudentAttendanceApprovalController {
    private final StudentAttendanceApprovalService studentAttendanceApprovalService;

    @PostMapping("/new")
    @Operation(summary = "수강생 출결 승인 등록 (수강생 토큰)")
    public CommonResult setStudentAttendanceApproval(@RequestBody StudentAttendanceApprovalRequest request) {
        studentAttendanceApprovalService.setStudentAttendanceApproval(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 본인 출결 승인 기록확인
    @GetMapping("/my-attendance-history/{month}")
    @Operation(summary = "(수강생) 출결 히스토리 확인(월별)")
    public ListResult<StudentAttendanceApprovalMonthly> getMyAttendanceApprovals(@PathVariable String month) {
        return ListConvertService.settingListResult(studentAttendanceApprovalService.getMyAttendanceApprovalsMonthly(month));
    }

    // 수강생 본인 출결 승인 기록확인
    @GetMapping("/my-attendance-history")
    @Operation(summary = "(수강생) 출결승인 요청 리스트")
    public ListResult<StudentAttendanceApprovalMonthly> getMyAttendanceApprovals() {
        return ListConvertService.settingListResult(studentAttendanceApprovalService.getMyAttendanceApprovals());
    }

    // 수강생 본인 출결 승인 기록확인
    @GetMapping("/my-attendance-history/page/{pageNum}")
    @Operation(summary = "(수강생) 출결승인 요청 리스트 페이징")
    public ListResult<StudentAttendanceApprovalMonthly> getMyAttendanceApprovals(@PathVariable int pageNum) {
        return studentAttendanceApprovalService.getMyAttendanceApprovals(pageNum);
    }

    @GetMapping("/my-attendance-history/find-date")
    @Operation(summary =  "(수강생) 출결 히스토리 확인(날짜 범위)")
    public ListResult<StudentAttendanceApprovalResponse> getMyAttendanceApprovalsDate(
            @RequestParam String dataStart , @RequestParam(required = false) String dateEnd) {
        return ListConvertService.settingListResult(studentAttendanceApprovalService.getMyAttendanceApprovalOfDate(dataStart,dateEnd));
    }
    // 수강생 출결 승인 R
    @GetMapping("/all/{state}")
    @Operation(summary = "수강생 출결 승인 리스트/ State 값 [전체보기 = 전체 , 승인대기 = 대기 , 승인완료 = 완료]")
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovals(@PathVariable long state) {
        return ListConvertService.settingListResult(studentAttendanceApprovalService.getStudentAttendanceApprovals(state));
    }
    // 수강생 출결 승인 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "수강생 출결 승인 리스트 (페이징)")
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovalsPage(@PathVariable int pageNum) {
        return studentAttendanceApprovalService.getStudentAttendanceApprovalsPage(pageNum);
    }
    //수강생 출결 승인 단수 R
    @GetMapping("detail/{id}")
    @Operation(summary = "수강생 출결 승인 상세보기")
    public SingleResult<StudentAttendanceApprovalResponse> getStudentAttendanceApprovalDetail(@PathVariable long id){
        return studentAttendanceApprovalService.getStudentAttendanceApprovalDetail(id);
    }
    // 수강생 출결 승인 U
    @PutMapping("/changeInfo/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 정보 수정")
    public CommonResult putStudentAttendanceApproval(@PathVariable long id, @RequestBody StudentAttendanceApprovalFixRequest request) {
        studentAttendanceApprovalService.putStudentAttendanceApproval(id, request);
        return ResponseService.getSuccessResult();
    }
    // 수강생 출결 승인 U
    @PutMapping("/change-approval/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 하기")
    public CommonResult putStudentAttendanceApprovalType(@PathVariable long id) {
        studentAttendanceApprovalService.putStudentAttendanceApprovalAdmin(id);
        return ResponseService.getSuccessResult();
    }

    /**
     * 수강생 출결 승인 수정 학생버전
     */
    @PutMapping("/change-approval/student-attendance-approval-id-by-student/{id}")
    @Operation(summary = "수강생 출결 승인 수정 학생버전")
    public CommonResult putStudentAttendanceApprovalChangeInfoByStudent(@PathVariable long id, @RequestBody StudentAttendanceApprovalChangeInfoByStudentRequest request) {
        studentAttendanceApprovalService.putStudentAttendanceApprovalChangeInfoByStudent(id, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping(value = "/change-add-file/approval-id/{id}",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "수강생 첨부 파일 등록")
    public CommonResult putStudentAttendanceApprovalChangeAddFile(@PathVariable long id, @RequestParam MultipartFile multipartFile) throws IOException {
        studentAttendanceApprovalService.putStudentAttendanceApprovalChangeAddFile(multipartFile, id);
        return ResponseService.getSuccessResult();
    }


    // 수강생 출결 승인 D
    @DeleteMapping("/delete/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 정보 삭제")
    public CommonResult delStudentAttendanceApproval(@PathVariable long id) {
        studentAttendanceApprovalService.delStudentAttendanceApproval(id);
        return ResponseService.getSuccessResult();
    }
}
