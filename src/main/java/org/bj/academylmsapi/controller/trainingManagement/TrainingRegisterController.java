package org.bj.academylmsapi.controller.trainingManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterItem;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterRequest;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterResponse;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.trainingManagement.TrainingRegisterService;
import org.springframework.web.bind.annotation.*;

// 훈련관리 - 훈련일지 등록 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trainingRegister")
@Tag(name = "TrainingRegister", description = "[훈련관리] 훈련일지 등록")
public class TrainingRegisterController {
    private final TrainingRegisterService trainingRegisterService;

    // 훈련일지 등록 C
    @PostMapping("/new")
    @Operation(summary = "훈련일지 등록")
    public CommonResult setTrainingRegister(@RequestBody TrainingRegisterRequest request) {
        trainingRegisterService.setTrainingRegister(request);
        return ResponseService.getSuccessResult();
    }

    /**
     * 훈련일지 등록 복수 R
     */
    @GetMapping("/all/subjectId/{subjectId}")
    @Operation(summary = "훈련일지 등록 리스트")
    public ListResult<TrainingRegisterItem> getTrainingRegisters(@PathVariable long subjectId) {
        return ListConvertService.settingListResult(trainingRegisterService.getTrainingRegisters(subjectId));
    }

    /**
     * 훈련일지 등록 복수 R 페이징
     */
    @GetMapping("/all/pageNum/{pageNum}/subjectId/{subjectId}")
    @Operation(summary = "훈련일지 등록 리스트 페이징")
    public ListResult<TrainingRegisterItem> getTrainingRegistersPage(@PathVariable int pageNum, @PathVariable long subjectId) {
        return trainingRegisterService.getTrainingRegistersPage(pageNum, subjectId);
    }

    // 훈련일지 등록 단수 R
    @GetMapping("/detail/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 상세보기")
    public SingleResult<TrainingRegisterResponse> getTrainingRegister(@PathVariable long trainingRegisterId) {
        return ResponseService.getSingleResult(trainingRegisterService.getTrainingRegister(trainingRegisterId));
    }

    // 훈련일지 등록 U
    @PutMapping("/changeInfo/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 정보 수정 (강사 토큰)")
    public CommonResult putTrainingRegister(@PathVariable long trainingRegisterId, @RequestBody @Valid  TrainingRegisterRequest request) {
        trainingRegisterService.putTrainingRegister(trainingRegisterId,request);
        return ResponseService.getSuccessResult();
    }

    // 훈련일지 등록 D
    @DeleteMapping("/delete/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 정보 삭제")
    public CommonResult delTrainingRegister(@PathVariable long trainingRegisterId) {
        trainingRegisterService.delTrainingRegister(trainingRegisterId);
        return ResponseService.getSuccessResult();
    }
}
