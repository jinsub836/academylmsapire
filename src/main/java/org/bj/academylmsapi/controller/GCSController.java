package org.bj.academylmsapi.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.student.StudentRequest;
import org.bj.academylmsapi.service.GCSService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.UploadService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/com")
public class GCSController {

    private final GCSService gcsService;
    private final UploadService uploadService;

    @PutMapping(value = "/api/gcs/upload/{resizedFile}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult objectUpload(@ModelAttribute MultipartFile resizedFile) throws IOException {
        uploadService.uploadImage(resizedFile);
        return ResponseService.getSuccessResult();
    }
}
