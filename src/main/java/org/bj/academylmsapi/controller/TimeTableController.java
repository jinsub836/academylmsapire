package org.bj.academylmsapi.controller;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.timeTable.TimeTableService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/time-table")
public class TimeTableController {
   private final TimeTableService timeTableService;

   @PostMapping("new")
   public CommonResult setTimeTable(@RequestParam("csvFile") MultipartFile multipartFile, @RequestParam("subjectId") Long id) throws IOException {
      timeTableService.setTimeTable(multipartFile,id);

      return ResponseService.getSuccessResult();
   }

}
