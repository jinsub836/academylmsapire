package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.subject.SubjectItem;
import org.bj.academylmsapi.model.subject.SubjectRequest;
import org.bj.academylmsapi.model.subject.SubjectTeacherItem;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.SubjectService;
import org.springframework.web.bind.annotation.*;

// 과정 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/subject")
@Tag(name = "Subject", description = "과정")
public class SubjectController {
    private final SubjectService subjectService;

    // 과정 C
    @PostMapping("/new")
    @Operation(summary = "과정 등록")
    public CommonResult setSubject(@RequestBody @Valid SubjectRequest request) {
        subjectService.setSubject(request);
        return ResponseService.getSuccessResult();
    }

    // 과정 복수 R
    @GetMapping("/all")
    @Operation(summary = "과정 목록 보기 ")
    public ListResult<SubjectItem> getSubjectList() {
        return ListConvertService.settingListResult(subjectService.getSubjectList());
    }

    @GetMapping("/all/teacher")
    @Operation(summary = "과정 목록 보기[강사용] TOKEN 사용")
    public ListResult<SubjectItem> getTeacherSubjectList() {
        return ListConvertService.settingListResult(subjectService.getTeacherSubjectList());
    }

    // 과정 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "[관리자용]과정 목록 보기 (페이징)")
    public ListResult<SubjectItem> getSubjectPageList(@PathVariable int pageNum) {
        return subjectService.getSubjectPageList(pageNum);
    }

    // 과정 복수 R 페이징
    @GetMapping("/all/teacher/pageNum/{pageNum}")
    @Operation(summary = "[강사용]과정 목록 보기 (페이징)")
    public ListResult<SubjectTeacherItem> getSubjectTeacherPageList(@PathVariable int pageNum) {
        return subjectService.getSubjectTeacherPageList(pageNum);
    }


    // 과정 U
    @PutMapping("/changeInfo/subjectId/{subjectId}")
    @Operation(summary = "과정 정보 수정")
    public CommonResult putSubject(@PathVariable long subjectId, @RequestBody @Valid SubjectRequest subjectRequest) {
        subjectService.putSubject(subjectId, subjectRequest);
        return ResponseService.getSuccessResult();
    }

    // 과정 D
    @DeleteMapping("/delete/subjectId/{subjectId}")
    @Operation(summary = "과정 정보 삭제")
    private CommonResult delSubject(@PathVariable long subjectId) {
        subjectService.delSubject(subjectId);
        return ResponseService.getSuccessResult();
    }
}
