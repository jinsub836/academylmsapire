package org.bj.academylmsapi.exception;

public class CExamException extends RuntimeException {
    public CExamException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExamException(String msg) {
        super(msg);
    }
}
