package org.bj.academylmsapi.exception;

public class CMemberUsernamePatternException extends RuntimeException {
    public CMemberUsernamePatternException(String msg, Throwable t) {
        super(msg,t);
    }
    public CMemberUsernamePatternException(String msg) {
        super(msg);
    }
    public CMemberUsernamePatternException() {
        super();
    }
}
