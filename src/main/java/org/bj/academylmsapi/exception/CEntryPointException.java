package org.bj.academylmsapi.exception;

public class CEntryPointException extends RuntimeException{
    public CEntryPointException(String msg, Throwable t) {
        super(msg,t);
    }
    public CEntryPointException(String msg) {
        super(msg);
    }
    public CEntryPointException() {
        super();
    }
}
