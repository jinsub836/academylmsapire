package org.bj.academylmsapi.exception;

// 이름은 예외 처리하는 이름을 사용한다 지금은 회원에 관한 예외처리라 멤버를 썼다.
// 이떄 이름은 상세하게 길어져도 상관없고 직관적으로 이름만 보고 어느 예외 인지 알수 있어야한다.
public class CMemberException extends RuntimeException {
    public CMemberException(String msg, Throwable t) {
        super(msg, t);
    }
    public CMemberException(String msg) {
        super(msg);
    }
    public CMemberException() {
        super();
    }
}
