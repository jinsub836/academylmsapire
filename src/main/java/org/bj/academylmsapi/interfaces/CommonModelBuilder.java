package org.bj.academylmsapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
