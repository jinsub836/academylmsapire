package org.bj.academylmsapi.entity.trainingManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.trainingManagement.TrainingRegisterRequest;

import java.time.LocalDate;

// 훈련관리 - 훈련일지 등록 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 강사id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;

    // 과목id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "subjectId")
    private Subject subject;

    // 훈련날짜
    @Column(nullable = false)
    private LocalDate dateTraining;

    // 재적
    @Column(nullable = false)
    private Short registrationNum;

    // 출석
    private Short attendanceNum;

    // 이론
    @Column(nullable = false)
    private Short theory;

    // 실습
    @Column(nullable = false)
    private Short practice;

    // 결석
    @Column(columnDefinition = "TEXT")
    private String absentWho;

    // 지각
    @Column(columnDefinition = "TEXT")
    private String lateWho;

    // 조퇴
    @Column(columnDefinition = "TEXT")
    private String earlyLeaveWho;

    // 훈련내용
    @Column(columnDefinition = "TEXT")
    private String trainingContent;

    // 기타사항
    @Column(columnDefinition = "TEXT")
    private String etc;

    // 등록일
    @Column(nullable = false)
    private LocalDate dateRegister;

    public void putTrainingRegister(TrainingRegisterRequest request, Teacher teacher, Subject subject) {
        this.teacher = teacher;
        this.subject = subject;
        this.dateTraining = request.getDateTraining();
        this.registrationNum = request.getRegistrationNum();
        this.attendanceNum = request.getAttendanceNum();
        this.theory = request.getTheory();
        this.practice = request.getPractice();
        this.absentWho = String.join("@", request.getAbsentWho());
        this.lateWho = String.join("@", request.getLateWho());
        this.earlyLeaveWho = String.join("@", request.getEarlyLeaveWho());
        this.trainingContent = request.getTrainingContent();
        this.etc = request.getEtc();
    }

    private TrainingRegister(Builder builder) {
        this.teacher = builder.teacher;
        this.subject = builder.subject;
        this.dateTraining = builder.dateTraining;
        this.registrationNum = builder.registrationNum;
        this.attendanceNum = builder.attendanceNum;
        this.theory = builder.theory;
        this.practice = builder.practice;
        this.absentWho = builder.absentWho;
        this.lateWho = builder.lateWho;
        this.earlyLeaveWho = builder.earlyLeaveWho;
        this.trainingContent = builder.trainingContent;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegister> {
        private final Teacher teacher;
        private final Subject subject;
        private final LocalDate dateTraining;
        private final Short registrationNum;
        private final Short attendanceNum;
        private final Short theory;
        private final Short practice;
        private final String absentWho;
        private final String lateWho;
        private final String earlyLeaveWho;
        private final String trainingContent;
        private final String etc;
        private final LocalDate dateRegister;

        public Builder(TrainingRegisterRequest request, Teacher teacher, Subject subject) {
            this.teacher = teacher;
            this.subject = subject;
            this.dateTraining = request.getDateTraining();
            this.registrationNum = request.getRegistrationNum();
            this.attendanceNum = request.getAttendanceNum();
            this.theory = request.getTheory();
            this.practice = request.getPractice();
            this.absentWho = String.join("@",request.getAbsentWho());
            this.lateWho = String.join("@",request.getLateWho());
            this.earlyLeaveWho = String.join("@",request.getEarlyLeaveWho());
            this.trainingContent = request.getTrainingContent();
            this.etc = request.getEtc();
            this.dateRegister = LocalDate.now();
        }
        @Override
        public TrainingRegister build() {
            return new TrainingRegister(this);
        }
    }
}
