package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.teacher.TeacherInfoChangeAdminRequest;
import org.bj.academylmsapi.model.teacher.TeacherInfoChangeRequest;
import org.bj.academylmsapi.model.teacher.TeacherPasswordChangeRequest;
import org.bj.academylmsapi.model.teacher.TeacherRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

// 강사 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED) // 접근제어자 protected로 준다
public class Teacher implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 사진주소
    @Column(columnDefinition = "TEXT")
    private String imgSrc;

    // 이름
    @Column(nullable = false, length = 20)
    private String teacherName;


    // 강사 아이디
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    // 강사 비밀번호
    @Column(nullable = false, columnDefinition = "TEXT")
    private String password;

    // 회원 분류 및 등급
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberType memberType;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인휴대폰
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 이메일
    @Column(nullable = false, length = 50, unique = true)
    private String email;

    // 가입일
    @Column(nullable = false)
    private LocalDate dateJoin;

    /**
     * 강사 이미지 수정
     */
    public void putTeacherImage(String request) {
        this.imgSrc = request;
    }

    // 강사 비밀번호 U
    public void putTeacherPassword(TeacherPasswordChangeRequest request) {
        this.password = request.getPassword();
    }

    // 강사 U
    public void putTeacher(TeacherInfoChangeRequest request) {
        this.imgSrc = request.getImgSrc();
        this.teacherName = request.getTeacherName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
    }

    // 강사 U 관리자 버전
    public void putTeacherAdmin(TeacherInfoChangeAdminRequest request) {
        this.imgSrc = request.getImgSrc();
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.teacherName = request.getTeacherName();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
    }
    private Teacher(Builder builder) {
        this.imgSrc = builder.imgSrc;
        this.teacherName = builder.teacherName;
        this.username = builder.username;
        this.password = builder.password;
        this.memberType = builder.memberType;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.dateJoin = builder.dateJoin;

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class Builder implements CommonModelBuilder<Teacher> {
        private String imgSrc;
        private final String teacherName;
        private final String username;
        private final String password;
        private final MemberType memberType;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final String email;
        private final LocalDate dateJoin;


        public Builder(TeacherRequest request) {
            this.teacherName = request.getTeacherName();
            this.username = request.getUsername()+"_teacher";
            this.password = request.getPassword();
            this.memberType = MemberType.ROLE_TEACHER;
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.email = request.getEmail();
            this.dateJoin = LocalDate.now();
        }

        @Override
        public Teacher build() {
            return new Teacher(this);
        }
    }
}
