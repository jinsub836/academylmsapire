package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 강사 id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 상담제목
    @Column(nullable = false, length = 20)
    private String adviceTitle;

    // 상담날짜
    @Column(nullable = false)
    private LocalDate dateAdvice;

    // 상담내용
    @Column(nullable = false)
    private String adviceContent;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putAdviceManagement(AdviceManagementRequest request, Student student, Teacher teacher) {
        this.student = student;
        this.teacher = teacher;
        this.adviceTitle = request.getAdviceTitle();
        this.dateAdvice = request.getDateAdvice();
        this.adviceContent = request.getAdviceContent();
        this.dateRegister = LocalDateTime.now();
    }

    private AdviceManagement(Builder builder) {
        this.student = builder.student;
        this.teacher = builder.teacher;
        this.adviceTitle = builder.adviceTitle;
        this.dateAdvice = builder.dateAdvice;
        this.adviceContent = builder.adviceContent;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagement> {
        private final Student student;
        private final Teacher teacher;
        private final String adviceTitle;
        private final LocalDate dateAdvice;
        private final String adviceContent;
        private final LocalDateTime dateRegister;

        public Builder(AdviceManagementRequest request, Student student, Teacher teacher) {
            this.student = student;
            this.teacher = teacher;
            this.adviceTitle = request.getAdviceTitle();
            this.dateAdvice = request.getDateAdvice();
            this.adviceContent = request.getAdviceContent();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public AdviceManagement build() {
            return new AdviceManagement(this);
        }
    }
}
