package org.bj.academylmsapi.entity.studentAttendance;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 출석
    @Column(nullable = false, length = 5)
    private Integer studentAttendance;

    // 조퇴
    @Column(nullable = false, length = 5)
    private Integer studentOut;

    // 지각
    @Column(nullable = false, length = 5)
    private Integer studentLate;

    // 무단결석
    @Column(nullable = false, length = 5)
    private Integer studentAbsence;

    // 병가
    @Column(nullable = false, length = 5)
    private Integer studentSick;

    // 등록일
    @Column(nullable = false)
    private LocalDate dateRegister;


    public void putStudentAttendanceByLate(StudentAttendance studentAttendance){
        this.studentLate = studentAttendance.getStudentLate()+1;
    }
    public void putStudentAttendanceByEarly(StudentAttendance studentAttendance){
        this.studentOut = studentAttendance.getStudentOut()+1;
    }
    public void putStudentAttendanceByAbsent(StudentAttendance studentAttendance){
        this.studentAbsence = studentAttendance.getStudentAbsence()+1;
    }
    public void putStudentAttendanceByTrainReport(StudentAttendance studentAttendance){
        this.studentAttendance = studentAttendance.getStudentAttendance()+1;
    }

    public void putStudentAttendanceApproval(StudentAttendance studentAttendance , AttendanceType attendanceType){
        if (attendanceType.equals(AttendanceType.LATE)){
            this.studentLate = studentAttendance.getStudentLate()-1;}
        else if (attendanceType.equals(AttendanceType.EARLYLEAVE)) {
            this.studentOut = studentAttendance.getStudentOut()-1;
        } else {  this.studentAbsence = studentAttendance.getStudentAbsence()-1;}
    }



    public void putStudentAttendance(StudentAttendanceRequest request, Student student) {
        this.student = student;
        this.studentAttendance = request.getStudentAttendance();
        this.studentOut = request.getStudentOut();
        this.studentLate = request.getStudentLate();
        this.studentAbsence = request.getStudentAbsence();
        this.studentSick = request.getStudentSick();
    }

    private StudentAttendance(Builder builder) {
        this.student = builder.student;
        this.studentAttendance = builder.studentAttendance;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsence = builder.studentAbsence;
        this.studentSick = builder.studentSick;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendance> {
        private final Student student;
        private final Integer studentAttendance;
        private final Integer studentOut;
        private final Integer studentLate;
        private final Integer studentAbsence;
        private final Integer studentSick;
        private final LocalDate dateRegister;

        public Builder(Student student) {
            this.student = student;
            this.studentAttendance = 0;
            this.studentOut = 0;
            this.studentLate = 0;
            this.studentAbsence = 0;
            this.studentSick = 0;
            this.dateRegister = LocalDate.now();
        }

        @Override
        public StudentAttendance build() {
            return new StudentAttendance(this);
        }
    }
}
