package org.bj.academylmsapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

// 최종학력

@AllArgsConstructor
@Getter
@Schema(description = "COLLEGE = 대졸, HIGHSCHOOL = 고졸, MIDDLESCHOOL = 중졸, ELEMENTARYSCHOOL = 초졸")
public enum FinalEducation {

    COLLEGE("대졸"),
    HIGHSCHOOL("고졸"),
    MIDDLESCHOOL("중졸"),
    ELEMENTARYSCHOOL("초졸");

    private final String EducationName;
}
