package org.bj.academylmsapi.enums;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

// 승인상태 Enum

@AllArgsConstructor
@Getter
@Schema(description = "NOTAPPROVED = 승인 대기, ADMINAPPROVAL = 승인 완료")
public enum ApprovalState {
    NOTAPPROVED("승인 대기"),
    ADMINAPPROVAL("승인 완료");

    private final String approvalStateName;
}
