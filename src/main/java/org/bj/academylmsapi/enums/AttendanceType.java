package org.bj.academylmsapi.enums;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


// 출결유형 Enum

@AllArgsConstructor
@Getter
@Schema(description = "LATE = 지각, EARLYLEAVE = 조퇴, ABSENT = 결석")
public enum AttendanceType {
    LATE("지각"),
    EARLYLEAVE("조퇴"),
    ABSENT("결석");

    private final String attendanceTypeName;
}
